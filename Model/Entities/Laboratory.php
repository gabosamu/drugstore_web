<?php
    class Laboratory{
        public $id;
        public $name;
        public $create_at;
        public $status;

        
        /**
         * Get the value of name
         */ 
        public function getName()
        {
                return $this->name;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */ 
        public function setName($name)
        {
                $this->name = $name;

                return $this;
        }


        /**
         * Get the value of create_at
         */ 
        public function getCreate_at()
        {
                return $this->create_at;
        }

        /**
         * Set the value of create_at
         *
         * @return  self
         */ 
        public function setCreate_at($create_at)
        {
                $this->create_at = $create_at;

                return $this;
        }

        /**
         * Get the value of status
         */ 
        public function getStatus()
        {
                return $this->status;
        }

        /**
         * Set the value of status
         *
         * @return  self
         */ 
        public function setStatus($status)
        {
                $this->status = $status;

                return $this;
        }

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }
    }
?>