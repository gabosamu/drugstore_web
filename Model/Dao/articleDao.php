<?php    
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Article.php");
    class articleDao{
      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta  de artículos a través de procedimientos almacenados    
        //  * @author Iván Jojoa
        //  * @date 30/01/2019
           
        public function insert(Article $article){           
           try{                
              
                $stmt = $this->conexion->prepare("CALL searchArticleByReference (?);");
                $stmt->bindParam("1", $article->referen, PDO::PARAM_STR, 4000);                  
                    
                $stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt =  $this->conexion->prepare("CALL insertArticle (?, ?, ?, ?, ?, ?);");
                    $stmt->bindParam("1", $article->nameArticle, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $article->referen, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("3", $article->invimaReg, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("4", $article->create_at, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("5", $article->is_active, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("6", $article->idCat, PDO::PARAM_STR, 4000);                 
                        
                    $stmt->execute();
                    return true; 
                }                               
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }           
        }

         //  * @description Metodo que obtiene la referencia del articulo
        //  * @author Orlando Mavisoy
        //  * @date 19/01/2019
        
        public function allArticleByReference(){
            try{
               
                $stmt = $this->conexion->prepare("CALL searchArticleByName();");                                
                $stmt->execute();                
                $data = "";
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .= "<option value='" . $row['id'] . "'>" .$row['reference'].  "</option>";                   
                }  
                $out["option"]=$data;        
                return json_encode($out);              
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }   

        //  * @description Metodo que obtiene la referencia del articulo
        //  * @author Orlando Mavisoy
        //  * @date 19/01/2019
        
        public function allArticleByName(){
            try{
          
                $stmt =  $this->conexion->prepare("CALL searchArticleByName();");                                
                $stmt->execute();                
                $data = "";
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .= "<option value='" . $row['id'] . "'>" .$row['nameArticle'].  "</option>";                   
                }  
                $out["option"]=$data;        
                return json_encode($out);              
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }

        public function searchArticleAll()
        {
            try{
                      
                $stmt =  $this->conexion->prepare("CALL searchArticleAll();");
                $stmt->execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .            
                    "<td>" . $row["nameArticle"] . "</td>" .
                    "<td>" . $row["reference"] . "</td>" .                  
                    "<td>" . $row["idCat"] . "</td>" .
                    "<td>" . $row["create_at"] . "</td>" . 
                    
                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#updateVeh' 
                    onclick=\"$.updateArticle('" . $row["id"] . "','" . $row["nameArticle"] . "','"  . $row["reference"] . "','" .$row["invimaReg"] . "','" .$row["idCat"] . "','" .$row["create_at"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["is_active"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledArticle('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledArticle('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledArticle('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledArticle('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);

            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }  
        public function statusArticle(Article $article){
            try{
            
                $stmt = $this->conexion->prepare("CALL updateStatusArticle (?, ?);");
                $stmt->bindParam("1", $article->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $article->is_active, PDO::PARAM_STR, 4000); 
              
                $stmt->execute();   
                var_dump($article);                  
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        
        }
        //  * @description Metodo que actualiza un artículo
        //  * @author Iván Jojoa
        //  * @date 01/02/2019
        
        public function update(Article $article){           
            try{                
               
                $stmt =  $this->conexion->prepare("CALL updateArticle (?, ?, ?, ?, ?, ?);");
                $stmt->bindParam("1", $article->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $article->nameArticle, PDO::PARAM_STR, 4000);
                $stmt->bindParam("3", $article->referen, PDO::PARAM_STR, 4000); 
                $stmt->bindParam("4", $article->invimaReg, PDO::PARAM_STR, 4000); 
                $stmt->bindParam("5", $article->is_active, PDO::PARAM_STR, 4000);
                $stmt->bindParam("6", $article->idCat, PDO::PARAM_STR, 4000);                  
                         
                $stmt->execute();
                return true;                                            
             }catch(Exception $e){
                 die('Error: '. $e->getMessage());               
             }finally{
                $this->conexion = null;
             }     
        }
    }    
?>      