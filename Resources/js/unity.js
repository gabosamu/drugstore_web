/**
         * @deprecated
         * @version 1.0        
         * @description Validar inicio de sesión
         * @author Iván Jojoa
         * @date 19/01/2019
         */ 
        $(document).ready(function(){      
            $id = "";
            $.extend({
                 /*
                @description Funcion que envia los datos de unidades  para ser almacenados en base de datos
                @author Iván Jojoa
                @date 19/01/2019
                */
                addUnity: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/unityController.php",               
                        success: function (data) {
                             if((data==="") || (data==="false")){				   	
                                alert("La unidad ya existe");                                
                            }else{
                                alert("Registro exitoso");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }, 
                  /*
                @description actualiza los datos de unidades
                @author Iván Jojoa
                @date 01/02/2019
                */
                updateUnity: function(id, name, create_at){            
                    $('#modalUnity').modal('show');                                   
                    $("#name").val(name);          
                    $("#idunity").val(id);                  
                },
                
                 /*
                @description Función que inactiva un unidad
                @author Iván Jojoa
                @date 01/02/2019
                */
                disabledUnity: function(id){                   
                    var data={
                        option: "statusUnity",
                        id: id,
                        status: 0
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/unityController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Inactivación exitosa");  
                                location.reload();  
                            }else{
                                alert("Error en inactivación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que activa una unidad
                @author Iván Jojoa
                @date 01/02/2019
                */
                enabledUnity: function(id){                   
                    var data={
                        option: "statusUnity",
                        id: id,
                        status: 1
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/unityController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Activación exitosa");
                                location.reload();      
                            }else{
                                alert("Error en activación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que actualiza una unidad
                @author Iván Jojoa
                @date 01/02/2019
                */
                updateUnityData: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/unityController.php",               
                        success: function (data) {   
                            if(data==="1"){                                                    
                            alert("Registro actualizado exitosamente");             
                            location.reload();     
                            }                                                                              
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }
          });


 /*
            @description Envia los datos de proveedores para se almacenados en database
            @author Iván Jojoa
            @date 01/02/2019
            */
            $("#accept").on('click', function() {
                $id = $("#idunity").val();
                if($id.trim()===null || $id.trim()===undefined || $id.trim()===""){
                    data={
                        name : $("#name").val(),                       
                        option: "insert"
                    }         
                    $.addUnity(data);
                }else{
                    data={
                        id: $("#idunity").val(),
                        name : $("#name").val(),                     
                        option: "update"
                    }         
                    $.updateUnityData(data);
                }                
                
            });        
        });
        
