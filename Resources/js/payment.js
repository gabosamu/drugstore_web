/**
         * @deprecated
         * @version 1.0        
         * @description Validar inicio de sesión
         * @author Iván Jojoa
         * @date 08/02/2019
         */ 
        $(document).ready(function(){      
           
            $.extend({               
                addPayment: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/paymentController.php",               
                        success: function (data) {
                             if((data==="") || (data==="false")){                   
                                alert("El pago ya existe");                                
                            }else{
                                alert("Registro exitoso");              
                                location.reload();
                            }                                                       
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                },
           
            });

            $("#accept").on('click', function() {
                    data={
                        invoiceP : $("#invoiceP").val(),
                        nameClient : $("#nameClient").val(),
                        val : $("#val").val(),
                        invoiceC : $("#invoiceC").val(),


                        option: "insert"
                    }         
                    $.addPayment(data);
            })

          
        });