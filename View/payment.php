<?php 
    require_once 'Template/header.php';
    require_once '../Model/Dao/paymentDao.php';
?>
  
<div class="container">
  <h1>Pagos Recibidos</h1>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalPayment"> + </button> 
    <!-- Modal -->
    <div id="modalPayment" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pagos Recibidos</h4>
        </div>
        <div class="modal-body">
        <form id="form">
            <div class="form-group">
                <label for="invoiceP">Factura de Pagos <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="invoiceP">
            </div>
            <div class="form-group">
                <label for="nameClient">Nombre del Cliente <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="nameClient">
            </div>
            <div class="form-group">
                <label for="val">Valor <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="val" >
            </div>

            <div class="form-group">
                <label for="invoiceC">Factura de Crédito <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="invoiceC" >
            </div>  
            <div class="form-group">               
                <input type="hidden" class="form-control" id="id">
            </div>  
           
            <button type="button" disabled="true" class="btn btn-danger" id="accept">Aceptar</button>
        </form>
        </div>
        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

    <!-- Datatables-->
    <table id="paymentDT" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Factura de Pago</th>     
                <th>Nombre del Cliente</th>
                <th>Valor</th>
                <th>Factura de Crédito</th>
                <th>Fecha de Pago</th>               
                <th>Acción</th>
                <th>Estado</th>
            </tr>
        </thead>        
        <tbody id="tBodyPayment">
             
        </tbody> 
    </table>
</div>
<script src="../Resources/js/payment.js"></script>
<?php include 'Template/footer.php';?>

    