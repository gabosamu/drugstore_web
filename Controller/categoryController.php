<?php
	require_once("../Model/Dao/categoryDao.php");  
	require_once("../Model/Entities/Category.php");
	
	//  * @description Metodo que recibe los datos de categoría de artículos para ser almacenados en bd	
	//  * @author Iván Jojoa
	//  * @date 28/01/2019	
	 
	$Option = $_POST['option'];	
	if($Option=="insert"){
		$category = new Category();
		
		$category->name = $_POST['name'];
		$category->created_at =	date('Y-m-d H:i:s');
		$category->is_active = 1; 

		$categoryDao = new categoryDao();
		$sql =$categoryDao->insert($category);	
		echo $sql;		
	}
		//  * @description Metodo que recibe los datos del categoría editar el estado del categoría
	//  * @author Iván Jojoa
	//  * @date 30/01/2019	
	     
	if($Option=="statusCategory"){
		$category = new Category();
		$category->id =  $_POST['id'];
		$category->is_active =  $_POST['is_active'];		

		$categoryDao = new categoryDao();
		$sql = $categoryDao->statusCategory($category);	
		echo $sql;		
	}	

	//  * @description Metodo que recibe los datos del categoría para actualizar	
	//  * @author Iván Jojoa
	//  * @date 30/01/2019	

	if($Option=="update"){
		$category = new Category();

		$category->id = $_POST['id'];
		$category->name = $_POST['name'];      

		$categoryDao = new categoryDao();
		$sql =$categoryDao->update($category);	
		echo $sql;		
	}


?>
