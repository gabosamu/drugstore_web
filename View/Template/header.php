<?php
 if (!isset($_SESSION)) {
    session_start();
  }
if(isset ($_SESSION['id'])) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Drogueria DyM</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../Resources/style/bootstrap.min.css">
  <link rel="stylesheet" href="../Resources/style/datatablesBootstrap.min.css">
  <link rel="stylesheet" href="../Resources/style/bootstrap-select.min.css">
  <link rel="stylesheet" href="../Resources/css/estilos.css">
  <script src="../Resources/pluggin/jquery.min.js"></script>
  <script src="../Resources/pluggin/bootstrap.min.js"></script> 
  <script src="../Resources/pluggin/datatablesJquery.min.js"></script>
  <script src="../Resources/pluggin/datatablesBootstrap.min.js"></script>
  <script src="../Resources/pluggin/bootstrap-select.min.js"></script>
  <script src="../Resources/pluggin/all.min.js"></script>
  <script src="../Resources/js/datatables.js"></script>
 
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Drogueria DyM</a>
    </div>
    <ul class="nav navbar-nav">
	  <li class="active"><a href="main.php">Inicio</a></li>
	  <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Ingresos<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="sale.php">Factura de venta</a></li>  
          <li><a href="payment.php">Pagos recibidos</a></li>                  
        </ul>
      </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="../articles.php">Egresos<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="invoices.php">Factura de proveedores</a></li>
          <li><a href="#">Otros Pagos</a></li>          
        </ul>
      </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Contactos<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="providers.php">Proveedores</a></li>
          <li><a href="client.php">Clientes</a></li>
          <li><a href="administrator.php">Administradores</a></li>
        </ul>
    </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Inventario<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Listas de precios</a></li>
          <li><a href="#">Valor de inventario</a></li>          
        </ul>
    </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Catalogos<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="laboratory.php">Laboratorios</a></li>
          <li><a href="unity.php">Unidades</a></li> 
          <li><a href="category.php">Categoría de artículos</a></li>  
          <li><a href="article.php">Artículos</a></li>        
        </ul>
    </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Soporte técnico<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="laboratory.php">Contáctenos</a></li>
          <li><a href="unity.php">Chat</a></li>                 
        </ul>
    </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
     
      <li><a href="../index.php"><i class="fas fa-user-tie"></i> Salir</a></li>
    </ul>
  </div>
   
</nav>
<p style="color: black; text-align: right; font-weight:bold; padding-right: 20px;">
<?php  
echo "Usuario: ".  $_SESSION["name"].' '.$_SESSION["lastname"] ; ?> </p>
<?php
}else{
    echo "<script>window.location='../index.php';</script>";  
} 
?>




