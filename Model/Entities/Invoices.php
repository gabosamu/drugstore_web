<?php
	class Invoices{
		public $id;	
		public $bill;	
		public $create_at;
		public $lot;		
		public $quantity;		
        public $costPrice;
        public $salePrice;
		public $iva;
		public $expirationDate;
		public $totalPriceCost;
		public $totalPriceSale;
		public $is_active;
		public $typeInvoices;
		public $idprov;
		public $idunity;
		public $idlab;
		public $idart;
        
        /**
		 * Get the value of id
		 */ 
		public function getId()
		{
				return $this->id;
		}

		/**
		 * Set the value of id
		 *
		 * @return  self
		 */ 
		public function setId($id)
		{
				$this->id = $id;

				return $this;
		}

		/**
		 * Get the value of bill
		 */ 
		public function getBill()
		{
				return $this->bill;
		}

		/**
		 * Set the value of bill
		 *
		 * @return  self
		 */ 
		public function setBill($bill)
		{
				$this->bill = $bill;

				return $this;
		}

	

		/**
		 * Get the value of lot
		 */ 
		public function getLot()
		{
				return $this->lot;
		}

		/**
		 * Set the value of lot
		 *
		 * @return  self
		 */ 
		public function setLot($lot)
		{
				$this->lot = $lot;

				return $this;
		}

	   /**
		 * Get the value of quantity
		 */ 
		public function getQuantity()
		{
				return $this->quantity;
		}

		/**
		 * Set the value of quantity
		 *
		 * @return  self
		 */ 
		public function setQuantity($quantity)
		{
				$this->quantity = $quantity;

				return $this;
		}

	

        /**
         * Get the value of costPrice
         */ 
        public function getCostPrice()
        {
                return $this->costPrice;
        }

        /**
         * Set the value of costPrice
         *
         * @return  self
         */ 
        public function setCostPrice($costPrice)
        {
                $this->costPrice = $costPrice;

                return $this;
        }

        /**
         * Get the value of salePrice
         */ 
        public function getSalePrice()
        {
                return $this->salePrice;
        }

        /**
         * Set the value of salePrice
         *
         * @return  self
         */ 
        public function setSalePrice($salePrice)
        {
                $this->salePrice = $salePrice;

                return $this;
        }

		/**
		 * Get the value of iva
		 */ 
		public function getIva()
		{
				return $this->iva;
		}

		/**
		 * Set the value of iva
		 *
		 * @return  self
		 */ 
		public function setIva($iva)
		{
				$this->iva = $iva;

				return $this;
		}

		/**
		 * Get the value of expirationDate
		 */ 
		public function getExpirationDate()
		{
				return $this->expirationDate;
		}

		/**
		 * Set the value of expirationDate
		 *
		 * @return  self
		 */ 
		public function setExpirationDate($expirationDate)
		{
				$this->expirationDate = $expirationDate;

				return $this;
		}

		/**
		 * Get the value of totalPriceCost
		 */ 
		public function getTotalPriceCost()
		{
				return $this->totalPriceCost;
		}

		/**
		 * Set the value of totalPriceCost
		 *
		 * @return  self
		 */ 
		public function setTotalPriceCost($totalPriceCost)
		{
				$this->totalPriceCost = $totalPriceCost;

				return $this;
		}

		/**
		 * Get the value of totalPriceSale
		 */ 
		public function getTotalPriceSale()
		{
				return $this->totalPriceSale;
		}

		/**
		 * Set the value of totalPriceSale
		 *
		 * @return  self
		 */ 
		public function setTotalPriceSale($totalPriceSale)
		{
				$this->totalPriceSale = $totalPriceSale;

				return $this;
		}

       

        /**
         * Get the value of idprov
         */ 
        public function getIdprov()
        {
                return $this->idprov;
        }

        /**
         * Set the value of idprov
         *
         * @return  self
         */ 
        public function setIdprov($idprov)
        {
                $this->idprov = $idprov;

                return $this;
        }

		/**
		 * Get the value of idlab
		 */ 
		public function getIdlab()
		{
				return $this->idlab;
		}

		/**
		 * Set the value of idlab
		 *
		 * @return  self
		 */ 
		public function setIdlab($idlab)
		{
				$this->idlab = $idlab;

				return $this;
		}

		/**
		 * Get the value of idunity
		 */ 
		public function getIdunity()
		{
				return $this->idunity;
		}

		/**
		 * Set the value of idunity
		 *
		 * @return  self
		 */ 
		public function setIdunity($idunity)
		{
				$this->idunity = $idunity;

				return $this;
		}

		/**
		 * Get the value of create_at
		 */ 
		public function getCreate_at()
		{
				return $this->create_at;
		}

		/**
		 * Set the value of create_at
		 *
		 * @return  self
		 */ 
		public function setCreate_at($create_at)
		{
				$this->create_at = $create_at;

				return $this;
		}

		/**
		 * Get the value of typeInvoices
		 */ 
		public function getTypeInvoices()
		{
				return $this->typeInvoices;
		}

		/**
		 * Set the value of typeInvoices
		 *
		 * @return  self
		 */ 
		public function setTypeInvoices($typeInvoices)
		{
				$this->typeInvoices = $typeInvoices;

				return $this;
		}

		/**
		 * Get the value of is_active
		 */ 
		public function getIs_active()
		{
				return $this->is_active;
		}

		/**
		 * Set the value of is_active
		 *
		 * @return  self
		 */ 
		public function setIs_active($is_active)
		{
				$this->is_active = $is_active;

				return $this;
		}

		/**
		 * Get the value of idart
		 */ 
		public function getIdart()
		{
				return $this->idart;
		}

		/**
		 * Set the value of idart
		 *
		 * @return  self
		 */ 
		public function setIdart($idart)
		{
				$this->idart = $idart;

				return $this;
		}
	}
?>