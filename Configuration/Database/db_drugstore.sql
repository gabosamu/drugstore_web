-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-01-2019 a las 21:16:07
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_drugstore`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarproducto` (`idpdr` INT, `imagepdr` VARCHAR(50), `barcodepdr` VARCHAR(50), `namepdr` VARCHAR(50), `descriptionpdr` TEXT, `inventary_minpdr` INT, `price_inpdr` FLOAT, `price_outpdr` FLOAT, `unitpdr` VARCHAR(255), `presentationpdr` VARCHAR(255), `user_idpdr` INT, `category_idpdr` INT, `created_atpdr` DATETIME, `is_activepdr` TINYINT)  BEGIN UPDATE product SET image=imagepdr,barcode=barcodepdr,name=namepdr,description=descriptionpdr,inventary_min=inventary_minpdr,price_in=price_inpdr,price_out=price_outpdr,
unit=unitpdr,presentation=presentationpdr,user_id=user_idpdr,category_id=category_idpdr,created_at=created_atpdr,is_active=is_activepdr WHERE id=idpdr;END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarusuario` (`idusr` INT, `nameusr` VARCHAR(50), `lstname` VARCHAR(50), `usrname` VARCHAR(50), `usrmail` VARCHAR(255), `usrpassword` VARCHAR(60), `usrimg` VARCHAR(255), `usr_active` TINYINT, `usr_admin` TINYINT, `usrcreated` DATETIME)  BEGIN UPDATE user SET name=nameusr,lastname=lstname,username=usrname,email=usrmail,password=usrpassword,image=usrimg,is_active=usr_active,is_admin=usr_admin,created_at=usrcreated WHERE id=idusr;END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_categoria` (`pid` INT, `pimage` VARCHAR(255), `pname` VARCHAR(50), `pdescription` TEXT, `pcreated_at` DATETIME, `pcondition` VARCHAR(45))  begin
update category set image=pimage,name=pname, description=pdescription,created_at=pcreated_at, conditio=pcondition WHERE id=pid;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_person` (`pid` INT, `pimage` VARCHAR(255), `pname` VARCHAR(255), `pcompany` VARCHAR(50), `paddress1` VARCHAR(50), `paddress2` VARCHAR(50), `pphone1` VARCHAR(50), `pphone2` VARCHAR(50), `pemail1` VARCHAR(50), `pemail2` VARCHAR(50), `pkind` INT, `pcreated_at` DATETIME)  begin
update person set image=pimage,name=pname, lastname=plastname,company=pcompany,address1=paddress1,address2=paddress2,phone1=pphone1,phone2=pphone2,email1=pemail1,email2=pemail2,kind=pkind,created_at=pcreated_at WHERE id=pid;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarproducto` (`idpdr` INT)  BEGIN SELECT * FROM product WHERE id=idpdr;END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarusuario` (`idusr` INT)  BEGIN SELECT * FROM user WHERE id=idusr;END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarUsuarioLogin` (`user` VARCHAR(50), `pass` VARCHAR(50))  BEGIN SELECT * FROM user WHERE username=user AND password = pass;END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_categoria` (`pid` INT)  BEGIN SELECT*from category WHERE id=pid; 
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_person` (`pid` INT)  BEGIN SELECT*from person WHERE id=pid; 
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarproducto` (`idpdr` INT)  BEGIN DELETE FROM product WHERE id=idpdr;END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarusuario` (`idusr` INT)  BEGIN DELETE FROM user WHERE id=idusr;end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_categoria` (`pid` INT)  BEGIN DELETE FROM category WHERE id=pid; 
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_person` (`pid` INT)  BEGIN DELETE FROM person WHERE id=pid; 
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ingresar_categoria` (`pid` INT, `pimage` VARCHAR(255), `pname` VARCHAR(50), `pdescription` TEXT, `pcreated_at` DATETIME, `pcondition` VARCHAR(45))  begin insert into category
(id,image,name, description,created_at, conditio)
values(pid, pimage, pname, pdescription, pcreated_at, pcondition);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ingresar_person` (IN `pid` INT, IN `pimage` VARCHAR(255), IN `pname` VARCHAR(255), IN `plastname` VARCHAR(50), IN `pcompany` VARCHAR(50), IN `paddress1` VARCHAR(50), IN `paddress2` VARCHAR(50), IN `pphone1` VARCHAR(50), IN `pphone2` VARCHAR(50), IN `pemail1` VARCHAR(50), IN `pemail2` VARCHAR(50), IN `pkind` INT, IN `pcreated_at` DATETIME)  begin
insert into person (id,image,name, lastname,company,address1,address2,phone1,phone2,email1,email2,kind,created_at)
values(pid, pimage, pname, plastname, pcompany, paddress1, paddress2, pphone1, pphone2, pemail1,pemail2,pkind,pcreated_at);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarproducto` (`idpdr` INT, `imagepdr` VARCHAR(50), `barcodepdr` VARCHAR(50), `namepdr` VARCHAR(50), `descriptionpdr` TEXT, `inventary_minpdr` INT, `price_inpdr` FLOAT, `price_outpdr` FLOAT, `unitpdr` VARCHAR(255), `presentationpdr` VARCHAR(255), `user_idpdr` INT, `category_idpdr` INT, `created_atpdr` DATETIME, `is_activepdr` TINYINT)  BEGIN INSERT INTO product (id,image,barcode,name,description,inventary_min,price_in,price_out,unit,presentation,user_id,category_id,created_at,is_active) VALUES (idpdr, imagepdr, barcodepdr, namepdr, descriptionpdr, inventary_minpdr,price_inpdr, price_outpdr , unitpdr, presentationpdr, user_idpdr, category_idpdr, created_atpdr, is_activepdr);END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarusuario` (`nameusr` VARCHAR(50), `lstname` VARCHAR(50), `usrname` VARCHAR(50), `usrmail` VARCHAR(255), `usrpassword` VARCHAR(60), `usrimg` VARCHAR(255), `usr_active` TINYINT, `usr_admin` TINYINT, `usrcreated` DATETIME)  BEGIN INSERT INTO user(name,lastname,username,email,password,image,is_active,is_admin,created_at) VALUES (nameusr,lstname,usrname,usrmail,usrpassword,usrimg,usr_active,usr_admin,usrcreated);end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertProvider` (`name` VARCHAR(100), `nit` VARCHAR(45), `address` VARCHAR(100), `city` VARCHAR(50), `phone` INT(20), `cellphone` INT(20), `email` VARCHAR(50), `created_at` DATE, `status` BIT)  BEGIN INSERT INTO provider (name,nit,address,city,phone,cellphone,email,created_at,status) VALUES (name, nit, address, city, phone, cellphone,email, created_at, status);END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `searchProvider` (`_name` VARCHAR(50), `_nit` VARCHAR(50))  BEGIN SELECT * FROM provider WHERE name=_name OR nit = _nit;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `box`
--

CREATE TABLE `box` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `condition` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `short` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `kind` int(11) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operation`
--

CREATE TABLE `operation` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `q` float DEFAULT NULL,
  `operation_type_id` int(11) DEFAULT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operation_type`
--

CREATE TABLE `operation_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `address1` varchar(50) DEFAULT NULL,
  `address2` varchar(50) DEFAULT NULL,
  `phone1` varchar(50) DEFAULT NULL,
  `phone2` varchar(50) DEFAULT NULL,
  `email1` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `kind` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `inventary_min` int(11) DEFAULT '10',
  `price_in` double DEFAULT NULL,
  `price_out` double DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `presentation` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provider`
--

CREATE TABLE `provider` (
  `idprov` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `nit` varchar(45) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `phone` int(20) DEFAULT NULL,
  `cellphone` int(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `status` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provider`
--

INSERT INTO `provider` (`idprov`, `name`, `nit`, `address`, `city`, `phone`, `cellphone`, `email`, `created_at`, `status`) VALUES
(1, 'orlando', NULL, 'mavisoy', 'pasto', 2, 3, 'zgov13@hotmail.com', '2019-01-27', b'1'),
(2, 'ovidio', NULL, 'la laguna', 'cali', 3, 3, 'zgov13@hotmail.com', '2019-01-12', b'1'),
(3, 'carlos', NULL, 'nn', 'pasto', 3, 3, 'zgov13@hotmail.com', '2019-01-18', b'1'),
(4, 'ley', '320', 'la laguna', 'cali', 3, 23, 'zgov13@hotmail.com', '2019-01-13', b'1'),
(5, 'alkosto', '89', 'asd', 'pasto', 23, 3, 'zgov13@hotmail.com', '2019-01-26', b'1'),
(6, 'exito', '900', 'la laguna', 'pasto', 23, 32, 'zgov13@hotmail.com', '2019-01-11', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sell`
--

CREATE TABLE `sell` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `operation_type_id` int(11) DEFAULT '2',
  `box_id` int(11) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `cash` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `created_at`) VALUES
(1, 'Orlando', 'Mavisoy', 'orlando', 'zgov13@hotmail.com', '1234', 'imagen', 1, 1, '2019-01-02 12:11:11'),
(2, 'Isabel', 'Guerrero', 'isabel', 'izab9116@hotmail.com', '1234', 'imagen', 1, 1, '2019-01-02 12:12:46');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `box`
--
ALTER TABLE `box`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `short` (`short`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `operation_type_id` (`operation_type_id`),
  ADD KEY `sell_id` (`sell_id`);

--
-- Indices de la tabla `operation_type`
--
ALTER TABLE `operation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`idprov`);

--
-- Indices de la tabla `sell`
--
ALTER TABLE `sell`
  ADD PRIMARY KEY (`id`),
  ADD KEY `box_id` (`box_id`),
  ADD KEY `operation_type_id` (`operation_type_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `person_id` (`person_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `box`
--
ALTER TABLE `box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `operation`
--
ALTER TABLE `operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `operation_type`
--
ALTER TABLE `operation_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `provider`
--
ALTER TABLE `provider`
  MODIFY `idprov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sell`
--
ALTER TABLE `sell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `operation`
--
ALTER TABLE `operation`
  ADD CONSTRAINT `operation_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `operation_ibfk_2` FOREIGN KEY (`operation_type_id`) REFERENCES `operation_type` (`id`),
  ADD CONSTRAINT `operation_ibfk_3` FOREIGN KEY (`sell_id`) REFERENCES `sell` (`id`);

--
-- Filtros para la tabla `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `sell`
--
ALTER TABLE `sell`
  ADD CONSTRAINT `sell_ibfk_1` FOREIGN KEY (`box_id`) REFERENCES `box` (`id`),
  ADD CONSTRAINT `sell_ibfk_2` FOREIGN KEY (`operation_type_id`) REFERENCES `operation_type` (`id`),
  ADD CONSTRAINT `sell_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `sell_ibfk_4` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
