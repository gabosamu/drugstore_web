<?php    
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Laboratory.php");
    class laboratoryDao{
      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta proveedores atravez de procedimientos almacenados    
        //  * @author Iván Jojoa
        //  * @date 22/01/2019
           
        public function insert(Laboratory $laboratory){    
            try{ 
                $stmt = $this->conexion->prepare("CALL searchLaboratoryByName(?);");
				$stmt->bindParam("1", $laboratory->name, PDO::PARAM_STR, 4000);								
					
                $stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt = $this->conexion->prepare("CALL insertLaboratory (?, ?, ?);");
                    $stmt->bindParam("1", $laboratory->name, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $laboratory->create_at, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("3", $laboratory->status, PDO::PARAM_STR, 4000); 			
                        
                    $stmt->execute();
                    return true; 
                }            					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
                $this->conexion = null;
			}           
        }

        //  * @description Metodo que obtiene todos los laboratorios
        //  * @author Orlando Mavisoy
        //  * @date 19/01/2019
        
        public function allLaboratory(){
            try{
                $stmt = $this->conexion->prepare("CALL searchLaboratory();");                                
                $stmt->execute();                
                $data = "";
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .= "<option value='" . $row['id'] . "'>" .$row['name'].  "</option>";                   
                }  
                $out["option"]=$data;        
                return json_encode($out);              
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }   


        public function searchLaboratoryAll()
        {
            try{
               
                $stmt =$this->conexion->prepare("CALL searchLaboratoryAll();");
                $stmt->execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .            
                    "<td>" . $row["name"] . "</td>" .
                    "<td>" . $row["create_at"] . "</td>" . 
                    
                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#updateVeh' 
                    onclick=\"$.updateLaboratory('" . $row["id"] . "','" . $row["name"]  . 
                    "','".$row["create_at"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["status"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledLaboratory('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledLaboratory('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledLaboratory('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledLaboratory('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }           
        }
//  * @description Metodo que actualiza el estado del laboratorio
        //  * @autor Iván Jojoa
        //  * @date 31/01/2019


        public function statusLaboratory(Laboratory $laboratory){
            try{
                
                $stmt = $this->conexion->prepare("CALL updateStatusLaboratory (?, ?);");
                $stmt->bindParam("1", $laboratory->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $laboratory->status, PDO::PARAM_STR, 4000); 
              
                $stmt->execute();                               
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }  
        }

        //  * @description Metodo que actualiza un laboratorio
        //  * @author Iván Jojoa
        //  * @date 31/01/2019
        
        public function update(Laboratory $laboratory){           
            try{      
               
                $stmt = $this->conexion->prepare("CALL updateLaboratory (?, ?);");
                $stmt->bindParam("1", $laboratory->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $laboratory->name, PDO::PARAM_STR, 4000);                                           
                         
                $stmt->execute();
                return true;                                            
             }catch(Exception $e){
                 die('Error: '. $e->getMessage());               
             }finally{
                $this->conexion = null;
             }     
        }
    }
?>    