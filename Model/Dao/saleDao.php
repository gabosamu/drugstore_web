<?php    
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Sale.php");
    class saleDao{
      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta factura de ventas atravez de procedimientos almacenados    
        //  * @author Iván Jojoa
        //  * @date 09/02/2019
           
        public function insert(Sale $sale){           
           try{                
			
                $stmt = $this->conexion->prepare("CALL searchSaleByNumber (?);");
				$stmt->bindParam("1", $sale->numberInvoice, PDO::PARAM_STR, 4000);
									
					
                $stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt = $this->conexion->prepare("CALL insertSale (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
                    $stmt->bindParam("1", $sale->idClient, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $sale->nameAdmin, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("3", $sale->numberInvoice, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("4", $sale->idInvoice, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("5", $sale->cost, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("6", $sale->quantity, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("7", $sale->discount, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("8", $sale->expiration, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("9", $sale->total, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("10", $sale->create_at, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("11", $sale->status, PDO::PARAM_STR, 4000);					
                        
                    $stmt->execute();
                    return true; 
                }            					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
				$this->conexion = null;
			}           
        }


                //  * @description Metodo que obtiene todos la factura y los almacena en una tabla
        //  * @author Iván Jojoa
        //  * @date 13/02/2019
       
        public function searchSaleAll()
        {
            try{
              
                $stmt =$this->conexion->prepare("CALL searchSaleAll();");
                $stmt->execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .            
                    "<td>" . $row["idClient"] . "</td>" .
                    "<td>" . $row["numberInvoice"] . "</td>" .
                    "<td>" . $row["idInvoice"] . "</td>" .
                    "<td>" . $row["cost"] . "</td>" .
                    "<td>" . $row["quantity"] . "</td>" .
                    "<td>" . $row["total"] . "</td>" . 
                    "<td>" . $row["create_at"] . "</td>" . 
                    
                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#updateVeh' 
                    onclick=\"$.updateSale('" . $row["id"] . "','" . $row["idClient"] . "','" .
                    $row["nameAdmin"] . "','" . $row["numberInvoice"] . "','" . $row["idInvoice"] . "','" . 
                    $row["cost"] . "','" . $row["quantity"] . "','" . $row["discount"] ."','" . $row["expiration"] ."','" .$row["total"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["status"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledSale('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledSale('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledSale('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledSale('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }           
        }
        //  * @description Metodo que actualiza el estado de la factura
        //  * @author Iván Jojoa
        //  * @date 13/02/2019


            public function statusSale(Sale $sale){
            try{
              
                $stmt =$this->conexion->prepare("CALL updateStatusSale (?, ?);");
                $stmt->bindParam("1", $sale->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $sale->status, PDO::PARAM_STR, 4000); 
              
                $stmt->execute();   
                var_dump($sale);                  
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        
        }
        //  * @description Metodo que actualiza un factura
        //  * @author Iván Jojoa
        //  * @date 13/02/2019
        
        public function update(Sale $sale){           
            try{                
                             
                $stmt = $this->conexion->prepare("CALL updateSale (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, );");
                $stmt->bindParam("1", $sale->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $sale->idClient, PDO::PARAM_STR, 4000);
                $stmt->bindParam("3", $sale->nameAdmin, PDO::PARAM_STR, 4000); 
                $stmt->bindParam("4", $sale->numberInvoice, PDO::PARAM_STR, 4000); 
                $stmt->bindParam("5", $sale->idInvoice, PDO::PARAM_STR, 4000);
                $stmt->bindParam("6", $sale->cost, PDO::PARAM_STR, 4000);
                $stmt->bindParam("7", $sale->quantity, PDO::PARAM_STR, 4000);
                $stmt->bindParam("8", $sale->discount, PDO::PARAM_STR, 4000);
                $stmt->bindParam("9", $sale->expiration, PDO::PARAM_STR, 4000);
                $stmt->bindParam("10", $sale->total, PDO::PARAM_STR, 4000);

                
                         
                $stmt->execute();
                return true;                                            
             }catch(Exception $e){
                 die('Error: '. $e->getMessage());               
             }finally{
                $this->conexion = null;
             }     
        }




    }
    
?>    