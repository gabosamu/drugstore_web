<?php    
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Payment.php");
    class paymentDao{
      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta Pagos atravez de procedimientos almacenados    
        //  * @author Iván Jojoa
        //  * @date 08/02/2019
           
        public function insert(Payment $payment){           
           try{                
			
                $stmt = $this->conexion->prepare("CALL searchPaymentByClient (?);");
				$stmt->bindParam("1", $payment->nameClient, PDO::PARAM_STR, 4000);
								
					
                $stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt = $this->conexion->prepare("CALL insertPayment (?, ?, ?, ?, ?, ?);");
                    $stmt->bindParam("1", $payment->invoiceP, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $payment->nameClient, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("3", $payment->val, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("4", $payment->invoiceC, PDO::PARAM_STR, 4000);

                    $stmt->bindParam("5", $payment->create_at, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("6", $payment->status, PDO::PARAM_STR, 4000);					
                        
                    $stmt->execute();
                    return true; 
                }            					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
				$this->conexion = null;
			}           
        }
    }    
?>