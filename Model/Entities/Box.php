<?php
	class Box{
		public $id;
		public $created_at;	

		public Box($id, $created_at)
		{
			$this->id = $id;
			$this->created_at = $created_at;
			
		}
		public function getId()
		{
		    return $this->id;
		}
		
		public function setId($id)
		{
		    $this->id = $id;
		    return $this;
		}

		public function getCreated_at()
		{
		    return $this->created_at;
		}
		
		public function setCreated_at($created_at)
		{
		    $this->created_at = $created_at;
		    return $this;
		}
	}
?>