<?php
	require_once("../Model/Dao/paymentDao.php");  
	require_once("../Model/Entities/Payment.php");
	
	//  * @description Metodo que recibe los datos de categoría de artículos para ser almacenados en bd	
	//  * @author Iván Jojoa
	//  * @date 08/02/2019	
	 
	$Option = $_POST['option'];	
	if($Option=="insert"){
		$payment = new Payment();
		
		$payment ->invoiceP = $_POST['invoiceP'];
		$payment ->nameClient = $_POST['nameClient'];
		$payment ->val = $_POST['val'];
		$payment ->invoiceC = $_POST['invoiceC'];
		$payment ->create_at =	date('Y-m-d H:i:s');
		$payment ->status = 1; 
		

		$paymentDao = new paymentDao();
		$sql =$paymentDao->insert($payment);	
		echo $sql;		
	}
?>