<?php 
    require_once 'Template/header.php';
    require_once '../Model/Dao/providerDao.php';
?>
  
<div class="container">
  <h1>Proveedores</h1>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalProvider"> + </button> 
    <!-- Modal -->
    <div id="modalProvider" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Registro proveedores</h4>
        </div>
        <div class="modal-body">
        <form id="form">
            <div class="form-group">
                <label for="name">Nombre comercial <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="name">
            </div>
            <div class="form-group">
                <label for="nit">NIT <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="nit">
            </div>
            <div class="form-group">
                <label for="address">Dirección <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="address" >
            </div>
            <div class="form-group">
                <label for="city">Ciudad <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="city" >
            </div>
            <div class="form-group">
                <label for="phone">Teléfono</label>
                <input type="text" required class="form-control" id="phone" >
            </div>
            <div class="form-group">
                <label for="cellphone">Celular <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="cellphone">
            </div>
            <div class="form-group">
                <label for="email">Email <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="email" >
            </div>
            <div class="form-group">
                <label for="fCreation">Fecha creación <span class="field_required">*</span></label>
                <input type="date" required class="form-control" id="fCreation" >
            </div>  
            <div class="form-group">               
                <input type="hidden" class="form-control" id="idprov">
            </div>  
           
            <button type="button" disabled="true" class="btn btn-danger" id="accept">Aceptar</button>
        </form>
        </div>
        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

    <!-- Datatables-->
    <table id="providerDT" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>     
                <th>Nit</th>
                <th>Dirección</th>
                <th>Ciudad</th>
                <th>Email</th>               
                <th>Celular</th>
                <th>Acción</th>
                <th>Estado</th>
            </tr>
        </thead>        
        <tbody id="tBodyProvider">
                <?php 
                    $provider = new providerDao();
                    $res=json_decode($provider->searchProviderAll());
                    echo $res->option;
                ?>                 
        </tbody> 
    </table>
</div>
<script src="../Resources/js/provider.js"></script>
<?php include 'Template/footer.php';?>