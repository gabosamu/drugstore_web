<?php
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Client.php");
    class clientDao{      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta clientes
        //  * @author Ivan Jojoa
        //  * @date 15/01/2019

        public function insert(Client $client){ 
            
           try{                
				$stmt = $this->conexion->prepare("CALL searchUser (?);");
                $stmt->bindParam("1", $client->numdoc, PDO::PARAM_STR, 4000);
                $stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt= $this->conexion->prepare("CALL insertUser(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
                    $stmt->bindParam("1", $client->name, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $client->lastname, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("3", $client->tipdoc, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("4", $client->numdoc, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("5", $client->gender, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("6", $client->address, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("7", $client->city, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("8", $client->telephone, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("9", $client->mobile, PDO::PARAM_STR, 4000);					
                    $stmt->bindParam("10",$client->email, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("11",$client->username, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("12",$client->password, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("13",$client->created_at, PDO::PARAM_STR, 4000);  
                    $stmt->bindParam("14",$client->profile, PDO::PARAM_STR, 4000);  
                    $stmt->bindParam("15",$client->status, PDO::PARAM_STR, 4000);                         
                    $stmt->execute();
                    return true; 
                }            					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
                $this->conexion = null;
			}           
        }
       
        //  * @description Metodo que obtiene todos los cliente
        //  * @author Ivan Jojoa
        //  * @date 15/01/2019

       public function searchUserAll()
        { 
            try{              

                $stmt = $this->conexion->prepare("CALL searchClientAll();");
                $stmt -> execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .            
                    "<td>" . $row["name"] . "</td>" .
                    "<td>" . $row["lastname"] . "</td>" .
                    "<td>" . $row["numdoc"] . "</td>" .
                    "<td>" . $row["mobile"] . "</td>" .
                    "<td>" . $row["email"] . "</td>" . 

                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#updateVeh' 
                    onclick=\"$.updateClient('" . $row["id"] . "','" . $row["name"] . "','" .
                    $row["lastname"] . "','" . $row["tipdoc"] . "','" . $row["numdoc"] . "','" . 
                    $row["gender"] . "','" . $row["address"] . "','" . $row["city"] . 
                    "','".$row["telephone"]."','".$row["mobile"]."','".$row["email"]."','".$row["created_at"]."','".$row["profile"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["status"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledClient('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledClient('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledClient('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledClient('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }  
        }      

        //  * @description Metodo que actualiza el estado del cliente
        //  * @author Iván Jojoa 
         //  * @date 18/01/2019  
         public function statusClient(Client $client){
            try{
                $stmt =  $this->conexion->prepare("CALL updateStatusUser (?, ?);");
                $stmt->bindParam("1", $client->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $client->status, PDO::PARAM_STR, 4000);                   
                    
                $stmt->execute();
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }

        //  * @description Metodo que actualiza un cliente
        //  * @author Iván Jojoa
        //  * @date 18/01/2019
        
        public function update(Client $client){           
            try{       
                $stmt = $this->conexion->prepare("CALL updateAllUser (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?, ?);");
                
                    $stmt->bindParam("1", $client->id, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $client->name, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("3", $client->lastname, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("4", $client->tipdoc, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("5", $client->numdoc, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("6", $client->gender, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("7", $client->address, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("8", $client->city, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("9", $client->telephone, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("10",$client->mobile, PDO::PARAM_STR, 4000);                   
                    $stmt->bindParam("11",$client->email, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("12",$client->username, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("13",$client->password, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("14",$client->created_at, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("15",$client->profile, PDO::PARAM_STR, 4000); 
                                  
                $stmt->execute();
                return true; 
                                            
             }catch(Exception $e){
                 die('Error: '. $e->getMessage());               
             }finally{
                $this->conexion = null;
             }           
         }
    } 
?>