<?php
	class Person{
		public $id;
		public $image;
		public $name;
		public $lastname;
		public $company;
		public $address1;
		public $address2;
		public $phone1;
		public $phone2;
		public $email1;
		public $email2;
		public $kind;
		public $created_at;

		function Person($id, $image, $name, $lastname, $company, $address1, $address2, $phone1, $phone2, $email1, $email2, $kind, $created_at)
		{
			$this->id = $id;
			$this->image = $image;
			$this->name = $name;
			$this->lastname = $lastname;
			$this->company = $company;
			$this->address1 = $address1;
			$this->address2= $address2;
			$this->phone1 = $phone1;
			$this->phone2 = $phone2;
			$this->email1 = $email1;
			$this->email2= $email2;
			$this->kind = $kind;
			$this->created_at = $created_at;						

		}

		public function getId()
		{
		    return $this->id;
		}
		
		public function setId($id)
		{
		    $this->id = $id;
		    return $this;
		}

		public function getImage()
		{
		    return $this->image;
		}
		
		public function setImage($image)
		{
		    $this->image = $image;
		    return $this;
		}

		public function getName()
		{
		    return $this->name;
		}
		
		public function setName($name)
		{
		    $this->name = $name;
		    return $this;
		}

		public function getLastname()
		{
		    return $this->lastname;
		}
		
		public function setLastname($lastname)
		{
		    $this->lastname = $lastname;
		    return $this;
		}

		public function getCompany()
		{
		    return $this->company;
		}
		
		public function setCompany($company)
		{
		    $this->company= $company;
		    return $this;
		}

		public function getAddress1()
		{
		    return $this->address1;
		}
		
		public function setAddress1($address1)
		{
		    $this->address1 = $address1;
		    return $this;
		}

		public function getAddress2()
		{
		    return $this->address2;
		}
		
		public function setAddress2($address2)
		{
		    $this->address2 = $address2;
		    return $this;
		}

		public function getPhone1()
		{
		    return $this->phone1;
		}
		
		public function setPhone1($phone1)
		{
		    $this->phone1 = $phone1;
		    return $this;
		}

		public function getPhone2()
		{
		    return $this->phone2;
		}
		
		public function setPhone2($phone2)
		{
		    $this->phone2 = $phone2;
		    return $this;
		}

		public function getEmail1()
		{
		    return $this->email1;
		}
		
		public function setEmail1($email1)
		{
		    $this->email1 = $email1;
		    return $this;
		}

		public function getEmail2()
		{
		    return $this->email2;
		}
		
		public function setEmail2($email2)
		{
		    $this->email2 = $email2;
		    return $this;
		}

		public function getKind()
		{
		    return $this->kind;
		}
		
		public function setKind($kind)
		{
		    $this->kind = $kind;
		    return $this;
		}

		public function getCreated_at()
		{
		    return $this->created_at;
		}
		
		public function setCreated_at($created_at)
		{
		    $this->created_at = $created_at;
		    return $this;
		}



		
		

	}
?>