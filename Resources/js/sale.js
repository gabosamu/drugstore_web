/*
            @description Funcion que envia los datos de factura para ser almacenados en base de datos
            @author Iván Jojoa
            @date 10/02/2019
        */
        $(document).ready(function(){      
           
            $.extend({        



                addSale: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/saleController.php",               
                        success: function (data) {
                             if((data==="") || (data==="false")){				   	
                                alert("La factura ya existe");                                
                            }else{
                                alert("Registro exitoso");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }, 

            /*
                @description actualiza los datos de la factura
                @author Iván Jojoa
                @date 13/02/2019
                */
                updateSale: function(id, idClient,nameAdmin, numberInvoice, idInvoice, cost, quantity, discount, expiration, total){            
                    $('#modalSale').modal('show');                                   
                    $("#idClient").val(idClient);
                    $("#nameAdmin").val(nameAdmin);
                    $("#numberInvoice").val(numberInvoice);
                    $("#idInvoice").val(idInvoice);
                    $("#cost").val(cost);
                    $("#quantity").val(quantity);
                    $("#discount").val(discount);
                    $("#expiration").val(expiration);
                    $("#total").val(total);
                     
                    $("#idsale").val(id);                  
                },
                
                 /*
                @description Función que inactiva una factuta
                @author Iván Jojoa
                @date 12/01/2019
                */
                disabledSale: function(id){                   
                    var data={
                        option: "statusSale",
                        id: id,
                        status: 0
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/saleController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Inactivación exitosa");  
                                location.reload();  
                            }else{
                                alert("Error en inactivación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que activa un factura
                @author Iván Jojoa
                @date 13/02/2019
                */
                enabledSale: function(id){                   
                    var data={
                        option: "statusSale",
                        id: id,
                        status: 1
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/saleController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Activación exitosa");
                                location.reload();      
                            }else{
                                alert("Error en activación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que actualiza un factura
                @author Iván Jojoa
                @date 13/02/2019
                */
                updateSaleData: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/saleController.php",               
                        success: function (data) {
                                                         
                                alert("Registro actualizado exitosamente");             
                                location.reload();
                                                                                
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }
            });          
                        
            /*
            @description Envia los datos de factura para se almacenados en database
            @author Iván Jojoa
            @date 13/02/2019
            */
            $("#accept").on('click', function() {
                $id = $("#idsale").val();
                if($id.trim()===null || $id.trim()===undefined || $id.trim()===""){
                    data={
                        idClient : $("#idClient").val(),
                        nameAdmin : $("#nameAdmin").val(),
                        numberInvoice : $("#numberInvoice").val(),
                        idInvoice : $("#idInvoice").val(),
                        cost : $("#cost").val(), 
                        quantity: $("#quantity").val(),
                        discount : $("#discount").val(),
                        expiration : $("#expiration").val(),
                        total: $("#total").val(),
                        option: "insert"
                    }         
                    $.addSale(data);
                }else{
                    data={
                        id: $("#idsale").val(),
                        idClient : $("#idClient").val(),
                        nameAdmin : $("#nameAdmin").val(),
                        numberInvoice : $("#numberInvoice").val(),
                        idInvoice : $("#idInvoice").val(),
                        cost : $("#cost").val(), 
                        quantity: $("#quantity").val(),
                        discount : $("#discount").val(),
                        expiration : $("#expiration").val(),
                        total: $("#total").val(),
                        
                        option: "update"
                    }         
                    $.updateSaleData(data);
                }                
                
            });        
        });
        
        
        
        
        
        