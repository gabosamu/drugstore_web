<?php
	class Sell{
		public $id;
		public $person_id;
		public $user_id;
		public $operation_type_id;
		public $box_id;
		public $total;
		public $cash;
		public $discount;
		public $created_at;


		public Sell($id, $person_id, $user_id, $operation_type_id, $box_id, $total, $cash, $discount, $created_at)
		{
			$this->id = $id;
			$this->person_id = $person_id;
			$this->user_id = $user_id;
			$this->operation_type_id = $operation_type_id;
			$this->box_id= $box_id;
			$this->total = $total;
			$this->cash= $cash;
			$this->discount = $discount;
			$this->created_at= $created_at;
							

		}
		public function getId()
		{
		    return $this->id;
		}
		
		public function setId($id)
		{
		    $this->id = $id;
		    return $this;
		}

		public function getPerson_id()
		{
		    return $this->person_id;
		}
		
		public function setPerson_in($person_id)
		{
		    $this->person_id = $person_id;
		    return $this;
		}

		public function getUser_id()
		{
		    return $this->user_id;
		}
		
		public function setUser_id($user_id)
		{
		    $this->user_id = $user_id;
		    return $this;
		}

		public function getOperation_type_id()
		{
		    return $this->operation_type_id;
		}
		
		public function setOperation_type_id($operation_type_id)
		{
		    $this->operation_type_id = $operation_type_id;
		    return $this;
		}

		public function getBox_id()
		{
		    return $this->box_id;
		}
		
		public function setBox_id($box_id)
		{
		    $this->box_id= $box_id;
		    return $this;
		}

		public function getTotal()
		{
		    return $this->total;
		}
		
		public function setTotal($total)
		{
		    $this->total = $total;
		    return $this;
		}

		public function getCash()
		{
		    return $this->cash;
		}
		
		public function setCash($cash)
		{
		    $this->cash = $cash;
		    return $this;
		}

		public function getDiscount()
		{
		    return $this->discount;
		}
		
		public function setDiscount($discount)
		{
		    $this->discount = $discount;
		    return $this;
		}

		public function getCreated_at()
		{
		    return $this->created_at;
		}
		
		public function setCreated_at($created_at)
		{
		    $this->created_at = $created_at;
		    return $this;
		}


	}
?>