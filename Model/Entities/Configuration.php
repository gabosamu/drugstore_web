<?php
	class Configuration{
		public $id;
		public $short;
		public $name;
		public $kind;
		public $val;

		public Configuration($id, $short, $name, $kind, $val)
		{
			$this->id = $id;
			$this->short = $short;
			$this->name = $name;
			$this->kind = $kind;
			$this->val = $val;
		}
		public function getId()
		{
		    return $this->id;
		}
		
		public function setId($id)
		{
		    $this->id = $id;
		    return $this;
		}

		public function getShort()
		{
		    return $this->short;
		}
		
		public function setShort($short)
		{
		    $this->short = $short;
		    return $this;
		}

		public function getName()
		{
		    return $this->name;
		}
		
		public function setName($name)
		{
		    $this->name = $name;
		    return $this;
		}

		public function getKind()
		{
		    return $this->kind;
		}
		
		public function setKind($kind)
		{
		    $this->kind = $kind;
		    return $this;
		}
		public function getVal()
		{
		    return $this->val;
		}
		
		public function setVal($val)
		{
		    $this->val = $val;
		    return $this;
		}

	}
?>