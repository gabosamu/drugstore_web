<?php
    class Provider{
        public $idprov;
        public $name;
        public $nit;
        public $address;
        public $city;
        public $phone;
        public $cellphone;
        public $email;
        public $create_at;
        public $status;
        
        /**
         * Get the value of name
         */ 
        public function getName()
        {
                return $this->name;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */ 
        public function setName($name)
        {
                $this->name = $name;

                return $this;
        }

        /**
         * Get the value of nit
         */ 
        public function getNit()
        {
                return $this->nit;
        }

        /**
         * Set the value of nit
         *
         * @return  self
         */ 
        public function setNit($nit)
        {
                $this->nit = $nit;

                return $this;
        }

        /**
         * Get the value of address
         */ 
        public function getAddress()
        {
                return $this->address;
        }

        /**
         * Set the value of address
         *
         * @return  self
         */ 
        public function setAddress($address)
        {
                $this->address = $address;

                return $this;
        }

        /**
         * Get the value of city
         */ 
        public function getCity()
        {
                return $this->city;
        }

        /**
         * Set the value of city
         *
         * @return  self
         */ 
        public function setCity($city)
        {
                $this->city = $city;

                return $this;
        }

        /**
         * Get the value of phone
         */ 
        public function getPhone()
        {
                return $this->phone;
        }

        /**
         * Set the value of phone
         *
         * @return  self
         */ 
        public function setPhone($phone)
        {
                $this->phone = $phone;

                return $this;
        }

        /**
         * Get the value of cellphone
         */ 
        public function getCellphone()
        {
                return $this->cellphone;
        }

        /**
         * Set the value of cellphone
         *
         * @return  self
         */ 
        public function setCellphone($cellphone)
        {
                $this->cellphone = $cellphone;

                return $this;
        }

        /**
         * Get the value of email
         */ 
        public function getEmail()
        {
                return $this->email;
        }

        /**
         * Set the value of email
         *
         * @return  self
         */ 
        public function setEmail($email)
        {
                $this->email = $email;

                return $this;
        }

        /**
         * Get the value of create_at
         */ 
        public function getCreate_at()
        {
                return $this->create_at;
        }

        /**
         * Set the value of create_at
         *
         * @return  self
         */ 
        public function setCreate_at($create_at)
        {
                $this->create_at = $create_at;

                return $this;
        }

        /**
         * Get the value of status
         */ 
        public function getStatus()
        {
                return $this->status;
        }

        /**
         * Set the value of status
         *
         * @return  self
         */ 
        public function setStatus($status)
        {
                $this->status = $status;

                return $this;
        }

      

        /**
         * Get the value of idprov
         */ 
        public function getIdprov()
        {
                return $this->idprov;
        }

        /**
         * Set the value of idprov
         *
         * @return  self
         */ 
        public function setIdprov($idprov)
        {
                $this->idprov = $idprov;

                return $this;
        }
    }
?>