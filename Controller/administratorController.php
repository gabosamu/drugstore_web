<?php
    require_once("../Model/Dao/administratorDao.php");  
    require_once("../Model/Entities/Administrator.php");
	
	//  * @description Metodo que recibe y envia parametros del administrador
	//  * @author Ivan Jojoa
	//  * @date 11/01/2019
	
    $Option = $_POST['option'];

	if($Option=="insert"){		
		$administrator = new Administrator();		
		$administrator->name = $_POST['name'];
		$administrator->lastname = $_POST['lastname'];
        $administrator->tipdoc = $_POST['tipdoc'];
        $administrator->numdoc = $_POST['numdoc'];	
        $administrator->gender= $_POST['gender'];
        $administrator->address = $_POST['address'];
        $administrator->city = $_POST['city'];
        $administrator->telephone = $_POST['telephone'];
        $administrator->mobile = $_POST['mobile'];
		$administrator->email= $_POST['email'];
		$administrator->username = $_POST['username'];
		$administrator->password = $_POST['password'];
		$administrator->created_at =date('Y-m-d H:i:s');
		$administrator->profile = $_POST['profile'];	
		$administrator->status = 1;
	

		$administratorDao = new administratorDao();  
		
		$sql =$administratorDao->insert($administrator);	
		echo $sql;		
	}
	//  * @description Metodo que recibe los datos del administrador editar el estado del administrador	
	//  * @author Iván Jojoa
	//  * @date 22/01/2019	
	     
	if($Option=="statusAdministrator"){
		$administrator = new Administrator();
		$administrator->id =  $_POST['id'];
		$administrator->status =  $_POST['status'];		

		$administratorDao = new administratorDao();
		$sql = $administratorDao->statusAdministrator($administrator);	
		echo $sql;		
	}	



	//  * @description Metodo que recibe los datos del administrador para actualizar	
	//  * @author Iván Jojoa
	//  * @date 18/01/2019	

	if($Option=="update"){
		$administrator = new Administrator();
		$administrator->id =  $_POST['id'];
		$administrator->name = $_POST['name'];
		$administrator->lastname = $_POST['lastname'];
        $administrator->tipdoc = $_POST['tipdoc'];
        $administrator->numdoc = $_POST['numdoc'];	
        $administrator->gender = $_POST['gender'];
        $administrator->address = $_POST['address'];
        $administrator->city = $_POST['city'];
        $administrator->telephone = $_POST['telephone'];
        $administrator->mobile = $_POST['mobile'];
		$administrator->email = $_POST['email'];
		$administrator->username = $_POST['username'];
		$administrator->password = $_POST['password'];
		$administrator->profile = $_POST['profile'];	
		$administrator->created_at = "";
		
		$administratorDao = new administratorDao();
		$sql = $administratorDao->update($administrator);	
		echo $sql;		
	}
?>