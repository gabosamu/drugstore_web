        /*
            @description Funcion que envia los datos de proveedores para ser almacenados en base de datos
            @author Orlando Mavisoy Guerrero
            @date 10/01/2019
        */
        $(document).ready(function(){      
           
            $.extend({               
                addProvider: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/providerController.php",               
                        success: function (data) {
                             if((data==="") || (data==="false")){				   	
                                alert("El proveedor ya existe");                                
                            }else{
                                alert("Registro exitoso");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }, 
               
                /*
                @description actualiza los datos de proveedores 
                @author Orlando Mavisoy Guerrero
                @date 10/01/2019
                */
                updateProvider: function(idprov, name, nit, address, city, phone, cellphone, email, created_at){            
                    $('#modalProvider').modal('show');                                   
                    $("#name").val(name);
                    $("#nit").val(nit);
                    $("#address").val(address);
                    $("#city").val(city);
                    $("#phone").val(phone);
                    $("#cellphone").val(cellphone);
                    $("#email").val(email);
                    $("#fCreation").val(created_at); 
                    $("#idprov").val(idprov);                  
                },
                
                 /*
                @description Función que inactiva un proveedor
                @author Orlando Mavisoy Guerrero
                @date 12/01/2019
                */
                disabledProvider: function(idprov){                   
                    var data={
                        option: "statusProvider",
                        idprov: idprov,
                        status: 0
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/providerController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Inactivación exitosa");	
                                location.reload();	
                            }else{
                                alert("Error en inactivación");
                            }                          											
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });			
                },

                /*
                @description Función que activa un proveedor
                @author Orlando Mavisoy Guerrero
                @date 12/01/2019
                */
                enabledProvider: function(idprov){                   
                    var data={
                        option: "statusProvider",
                        idprov: idprov,
                        status: 1
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/providerController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Activación exitosa");
                                location.reload();		
                            }else{
                                alert("Error en activación");
                            }                          											
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });			
                },

                /*
                @description Función que actualiza un proveedor
                @author Orlando Mavisoy Guerrero
                @date 16/01/2019
                */
                updateProviderData: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/providerController.php",               
                        success: function (data) {
                            if(data==="1"){	                              
                                alert("Registro actualizado exitosamente");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }
            });          
                        
            /*
            @description Envia los datos de proveedores para se almacenados en database
            @author Orlando Mavisoy Guerrero
            @date 10/01/2019
            */
            $("#accept").on('click', function() {
                $idprov = $("#idprov").val();
                if($idprov.trim()===null || $idprov.trim()===undefined || $idprov.trim()===""){
                    data={
                        name : $("#name").val(),
                        nit : $("#nit").val(),
                        address : $("#address").val(),
                        city : $("#city").val(),
                        phone : $("#phone").val(), 
                        cellphone : $("#cellphone").val(),
                        email : $("#email").val(),
                        fCreation : $("#fCreation").val(),
                        option: "insert"
                    }         
                    $.addProvider(data);
                }else{
                    data={
                        idprov: $("#idprov").val(),
                        name : $("#name").val(),
                        nit : $("#nit").val(),
                        address : $("#address").val(),
                        city : $("#city").val(),
                        phone : $("#phone").val(), 
                        cellphone : $("#cellphone").val(),
                        email : $("#email").val(),
                        fCreation : $("#fCreation").val(),
                        option: "update"
                    }         
                    $.updateProviderData(data);
                }                
                
            });        
        });
        
        
        
        
        
        