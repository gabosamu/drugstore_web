<?php
	class Administrator{
		public $id;
		public $name;
		public $lastname;
		public $tipdoc;
		public $numdoc;
		public $gender;
		public $address;
		public $city;
		public $telephone;
		public $mobile;
		public $email;
		public $username;
		public $password;
		public $created_at;
		public $profile;
		public $status;
						
        /**
         * Get the value of Name
         */ 	


		public function getName()
		{
		    return $this->name;
		}
		
        /**
         * Set the value of name
         *
         * @return  self
         */ 		
		public function setName($name)
		{
		    $this->name = $name;
		    return $this;
		}

        /**
         * Get the value of Lastname
         */ 	

		public function getLastname()
		{
		    return $this->lastname;
		}

        /**
         * Set the value of Lastname
         *
         * @return  self
         */ 

		
		public function setLastname($lastname)
		{
		    $this->lastname = $lastname;
		    return $this;
		}

        /**
         * Get the value of Tipdoc
         */ 

		public function getTipdoc()
		{
		    return $this->tipdoc;
		}

        /**
         * Set the value of Tipdoc
         *
         * @return  self
         */ 		

		public function setTipdoc($tipdoc)
		{
		    $this->tipdoc = $tipdoc;
		    return $this;
		}

        /**
         * Get the value of Numdoc
         */ 

		public function getNumdoc()
		{
		    return $this->numdoc;
		}

        /**
         * Set the value of Numdoc
         *
         * @return  self
         */ 		
		public function setNumdoc($numdoc)
		{
		    $this->numdoc= $numdoc;
		    return $this;
		}

        /**
         * Get the value of Gender
         */ 

		public function getGender()
		{
		    return $this->gender;
		}
        /**
         * Set the value of Gender
         *
         * @return  self
         */ 		

		
		public function setGender($gender)
		{
		    $this->gender = $gender;
		    return $this;
		}
        /**
         * Get the value of Address
         */ 


		public function getAddress()
		{
		    return $this->address;
		}
        /**
         * Set the value of Address
         *
         * @return  self
         */ 		

				
		public function setAddress($address)
		{
		    $this->address= $address;
		    return $this;
		}
        /**
         * Get the value of City
         */ 


		public function getCity()
		{
		    return $this->city;
		}

        /**
         * Set the value of City
         *
         * @return  self
         */ 		
		
		public function setCity($city)
		{
		    $this->city = $city;
		    return $this;
		}
        /**
         * Get the value of Telephone
         */ 


		public function getTelephone()
		{
		    return $this->telephone;
		}
        /**
         * Set the value of Telephone
         *
         * @return  self
         */ 				
		public function setTelephone($telephone)
		{
		    $this->telephone = $telephone;
		    return $this;
		}
        /**
         * Get the value of Mobile
         */ 


		public function getMobile()
		{
		    return $this->mobile;
		}
        /**
         * Set the value of Mobile
         *
         * @return  self
         */ 						

		public function setMobile($mobile)
		{
		    $this->mobile = $mobile;
		    return $this;
		}

        /**
         * Get the value of Email
         */ 

		public function getEmail()
		{
		    return $this->email;
		}

        /**
         * Set the value of Email
         *
         * @return  self
         */ 			


		public function setEmail($email)
		{
		    $this->email = $email;
		    return $this;
		}

        /**
         * Get the value of Username
         */ 


		public function getUsername()
		{
		    return $this->username;
		}

        /**
         * Set the value of Username
         *
         * @return  self
         */ 			


		public function setUsername($username)
		{
		    $this->username = $username;
		    return $this;
		}
        /**
         * Get the value of Password
         */ 


		public function getPassword()
		{
		    return $this->password;
		}

        /**
         * Set the value of Password
         *
         * @return  self
         */ 			
		
		public function setPassword($password)
		{
		    $this->password = $password;
		    return $this;
		}

        /**
         * Get the value of Created_at
         */ 

		public function getCreated_at()
		{
		    return $this->created_at;
		}

	     /**
         * Set the value of Created
         *
         * @return  self
         */ 	
		
		public function setCreated_at($created_at)
		{
		    $this->created_at = $created_at;
		    return $this;
		}		
        /**
         * Get the value of State
         */ 

		public function getStatus()
		{
		    return $this->status;
		}

	     /**
         * Set the value of State
         *
         * @return  self
         */ 			
		public function setStatus($status)
		{
		    $this->status = $status;
		    return $this;
		}	


        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
                $this->id = $id;

                return $this;

         }       

		/**
		 * Get the value of profile
		 */ 
		public function getProfile()
		{
				return $this->profile;
		}

		/**
		 * Set the value of profile
		 *
		 * @return  self
		 */ 
		public function setProfile($profile)
		{
				$this->profile = $profile;

				return $this;
		}
	}
?>