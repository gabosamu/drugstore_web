        /**          
         * @description Función que envia mediante ajax los datos del cliente
         * @author Ivan Jojoa
         * @date 17/01/2019
         */ 

        $(document).ready(function(){  
            $.extend({
                addClient: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/clientController.php",               
                        success: function (data) {
                             if((data==="") || (data==="false")){				   	
                                alert("El cliente ya existe");                                
                            }else{
                                alert("Registro exitoso");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                },
                
                 /*
                @description actualiza los datos de clientes 
                @author Iván Jojoa
                @date 18/01/2019
                */
                updateClient: function(id, name, lastname, tipdoc, numdoc, gender, address, city, telephone, mobile, email, created_at, profile){            
                    $('#modalClient').modal('show');                                   
                    $("#name").val(name),
                    $("#lastname").val(lastname),
                    $("#tipdoc").val(tipdoc),
                    $("#numdoc").val(numdoc),
                    $("#gender").val(gender), 
                    $("#address").val(address),
                    $("#city").val(city),
                    $("#telephone").val(telephone),
                    $("#mobile").val(mobile),
                    $("#email").val(email),                    
                    $("#created_at").val(created_at),  
                    $("#profile").val(profile), 
                    $("#idclient").val(id)                   
                },

                /*
                @description Función que actualiza un cliente
                @author Iván Jojoa
                @date 18/01/2019
                */
                updateClientData: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/clientController.php",               
                        success: function (data) {
                            if(data==="1"){                               
                                alert("Registro actualizado exitosamente");             
                                location.reload();
                            }                                                       
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }
            });  
            
             /**          
             * @description Función recibe los datos del cliente
             * @author Ivan Jojoa
             * @date 11/01/2019
            */                 
                             
            $("#accept").on('click', function() { 
                $id = $("#idclient").val();                           
                if($id.trim()===null || $id.trim()===undefined || $id.trim()===""){              
                    data={
                        name : $("#name").val(),
                        lastname : $("#lastname").val(),
                        tipdoc : $("#tipdoc").val(),
                        numdoc : $("#numdoc").val(),
                        gender : $("#gender").val(), 
                        address : $("#address").val(),
                        city : $("#city").val(),
                        telephone : $("#telephone").val(),
                        mobile : $("#mobile").val(),
                        email: $("#email").val(),    
                        profile: $("#profile").val(),                                      
                        option: "insert"
                    }  
                    $.addClient(data);
                }else{
                    data={
                        id: $("#idclient").val(),
                        name : $("#name").val(),
                        lastname : $("#lastname").val(),
                        tipdoc : $("#tipdoc").val(),
                        numdoc : $("#numdoc").val(),
                        gender : $("#gender").val(), 
                        address : $("#address").val(),
                        city : $("#city").val(),
                        telephone : $("#telephone").val(),
                        mobile : $("#mobile").val(),
                        email: $("#email").val(),    
                        profile: $("#profile").val(),                                       
                        option: "update"
                    }  
                    $.updateClientData(data);                  
                }  
            });    
        });
        
        
                     

        
        
        
        