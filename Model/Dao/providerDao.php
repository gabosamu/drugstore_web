<?php    
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Provider.php");
    class providerDao{
      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta proveedores atravez de procedimientos almacenados    
        //  * @author Orlando Mavisoy Guerrero
        //  * @date 10/01/2019
           
        public function insert(Provider $provider){           
           try{                
			
                $stmt = $this->conexion->prepare("CALL searchProvider (?, ?);");
				$stmt->bindParam("1", $provider->name, PDO::PARAM_STR, 4000);
				$stmt->bindParam("2", $provider->nit, PDO::PARAM_STR, 4000); 					
					
                $stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt = $this->conexion->prepare("CALL insertProvider (?, ?, ?, ?, ?, ?, ?, ?, ?);");
                    $stmt->bindParam("1", $provider->name, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $provider->nit, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("3", $provider->address, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("4", $provider->city, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("5", $provider->phone, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("6", $provider->cellphone, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("7", $provider->email, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("8", $provider->create_at, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("9", $provider->status, PDO::PARAM_STR, 4000);					
                        
                    $stmt->execute();
                    return true; 
                }            					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
				$this->conexion = null;
			}           
        }

        //  * @description Metodo que obtiene todos los proveedores y los almacena en una tabla
        //  * @author Orlando Mavisoy Guerrero
        //  * @date 10/01/2019
       
        public function searchProviderAll()
        {
            try{
              
                $stmt =$this->conexion->prepare("CALL searchProviderAll();");
                $stmt->execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .            
                    "<td>" . $row["name"] . "</td>" .
                    "<td>" . $row["nit"] . "</td>" .
                    "<td>" . $row["address"] . "</td>" .
                    "<td>" . $row["city"] . "</td>" .
                    "<td>" . $row["phone"] . "</td>" .
                    "<td>" . $row["phone"] . "</td>" . 
                    
                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#updateVeh' 
                    onclick=\"$.updateProvider('" . $row["idprov"] . "','" . $row["name"] . "','" .
                    $row["nit"] . "','" . $row["address"] . "','" . $row["city"] . "','" . 
                    $row["phone"] . "','" . $row["cellphone"] . "','" . $row["email"] . 
                    "','".$row["created_at"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["status"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnEnabled" . $row["idprov"] . "\" onclick=\"$.enabledProvider('" . $row["idprov"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnDisabled" . $row["idprov"] . "\" onclick=\"$.disabledProvider('" . $row["idprov"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnEnabled" . $row["idprov"] . "\" onclick=\"$.enabledProvider('" . $row["idprov"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnDisabled" . $row["idprov"] . "\" onclick=\"$.disabledProvider('" . $row["idprov"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
				$this->conexion = null;
			}           
        }

        //  * @description Metodo que actualiza el estado del proveedor
        //  * @author Orlando Mavisoy Guerrero
        //  * @date 12/01/2019


        public function statusProvider(Provider $provider){
            try{
              
                $stmt =$this->conexion->prepare("CALL updateStatusProvider (?, ?);");
                $stmt->bindParam("1", $provider->idprov, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $provider->status, PDO::PARAM_STR, 4000); 
              
                $stmt->execute();   
                var_dump($provider);                  
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        
        }
        //  * @description Metodo que actualiza un proveedor
        //  * @author Orlando Mavisoy Guerrero
        //  * @date 16/01/2019
        
        public function update(Provider $provider){           
            try{                
                             
                $stmt = $this->conexion->prepare("CALL updateProvider (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
                $stmt->bindParam("1", $provider->idprov, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $provider->name, PDO::PARAM_STR, 4000);
                $stmt->bindParam("3", $provider->nit, PDO::PARAM_STR, 4000); 
                $stmt->bindParam("4", $provider->address, PDO::PARAM_STR, 4000); 
                $stmt->bindParam("5", $provider->city, PDO::PARAM_STR, 4000);
                $stmt->bindParam("6", $provider->phone, PDO::PARAM_STR, 4000);
                $stmt->bindParam("7", $provider->cellphone, PDO::PARAM_STR, 4000);
                $stmt->bindParam("8", $provider->email, PDO::PARAM_STR, 4000);
                $stmt->bindParam("9", $provider->create_at, PDO::PARAM_STR, 4000);
                $stmt->bindParam("10", $provider->status, PDO::PARAM_STR, 4000);					
                         
                $stmt->execute();
                return true;                           					
             }catch(Exception $e){
                 die('Error: '. $e->getMessage());               
             }finally{
                $this->conexion = null;
             }     
        }

        //  * @description Metodo que obtiene el nombre del proveedor
        //  * @author Orlando Mavisoy
        //  * @date 19/01/2019
        
        public function allProviderByName(){
            try{
               
                $stmt =$this->conexion->prepare("CALL searchProviderByName();");                                
                $stmt->execute();                
                $data = "";
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .= "<option value='" . $row['idprov'] . "'>" .$row['name'].  "</option>";                   
                }  
                $out["option"]=$data;        
                return json_encode($out);              
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }     
    }    
?>