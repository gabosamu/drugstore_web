        /**          
         * @description Función que envia mediante ajax los datos del administrador
         * @author Ivan Jojoa
         * @date 11/01/2019
         */ 
        $(document).ready(function(){      

            $.extend({
                addAdministrator: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/administratorController.php",               
                        success: function (data) {
                             if((data==="") || (data==="false")){				   	
                                alert("El administrador ya existe");                                
                            }else{
                                alert("Registro exitoso");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                },      
               
                updateAdministrator: function(id, name, lastname, tipdoc, numdoc, gender, address, city, telephone, mobile, email, username, password, profile){                   
                    $('#modalAdministration').modal('show');                                   
                    $("#name").val(name),
                    $("#lastname").val(lastname),
                    $("#tipdoc").val(tipdoc),
                    $("#numdoc").val(numdoc),
                    $("#gender").val(gender), 
                    $("#address").val(address),
                    $("#city").val(city),
                    $("#telephone").val(telephone),
                    $("#cellphone").val(mobile),
                    $("#email").val(email),   
                    $("#username").val(username),
                    $("#password").val(password),   
                    $("#confirm-password").val(password),    
                    $("#profile").val(profile),           
                    $("#idadmon").val(id)                     
                },

                /*
                @description Función que inactiva un administrador
                @author Iván Jojoa
                @date 23/01/2019
                */
                disabledAdministrator: function(id){                   
                    var data={
                        option: "statusAdministrator",
                        id: id,
                        status: 0
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/administrationController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Inactivación exitosa");  
                                location.reload();  
                            }else{
                                alert("Error en inactivación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que activa un administrador
                @author Iván Jojoa
                @date 23/01/2019
                */
                enabledAdministrator: function(id){                   
                    var data={
                        option: "statusAdministrator",
                        id: id,
                        status: 1
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/administratorController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Activación exitosa");
                                location.reload();      
                            }else{
                                alert("Error en activación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                updateAdministratorData: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/administratorController.php",               
                        success: function (data) {
                            if(data==="1"){                               
                                alert("Registro actualizado exitosamente");             
                                location.reload();
                            }                                                       
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }

            });  



            /**          
             * @description Función recibe los datos del administrador
             * @author Ivan Jojoa
             * @date 11/01/2019
            */ 
            $("#accept").on('click', function() {
                $id = $("#idadmon").val();
                $pass = $("#password").val();
                $confPass = $("#confirm-password").val();
                if($pass.trim()!= ""){
                    if($pass===$confPass){             
                        if($id.trim()===null || $id.trim()===undefined || $id.trim()===""){
                            data={
                                    name : $("#name").val(),
                                    lastname : $("#lastname").val(),
                                    tipdoc : $("#tipdoc").val(),
                                    numdoc : $("#numdoc").val(),
                                    gender : $("#gender").val(), 
                                    address : $("#address").val(),
                                    city : $("#city").val(),
                                    telephone : $("#telephone").val(),
                                    mobile : $("#cellphone").val(),
                                    email: $("#email").val(),
                                    username: $("#username").val(), 
                                    profile: $("#profile").val(),
                                    password : $pass,                                                            
                                    option: "insert"
                            }         
                            $.addAdministrator(data);
                        }else{
                            data={
                                    id: $("#idadmon").val(),
                                    name : $("#name").val(),
                                    lastname : $("#lastname").val(),
                                    tipdoc : $("#tipdoc").val(),
                                    numdoc : $("#numdoc").val(),
                                    gender : $("#gender").val(), 
                                    address : $("#address").val(),
                                    city : $("#city").val(),
                                    telephone : $("#telephone").val(),
                                    mobile : $("#cellphone").val(),
                                    email: $("#email").val(),
                                    username: $("#username").val(), 
                                    profile: $("#profile").val(),
                                    password : $pass,                                                                  
                                    option: "update"
                            }         
                            $.updateAdministratorData(data);
                        }    
                    }
                }   
            });        
        });
        

                             











           