<?php include 'Template/header.php';
      require_once '../Model/Dao/clientDao.php';
?>
<div class="container">
  <h1>Clientes</h1>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalClient"> + </button> 
    <!-- Modal -->
    <div id="modalClient" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Registro cliente</h4>
        </div>
        <div class="modal-body">
        
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="name">Nombres <span class="field_required">*</span></label>
                    <input type="text" required="required" class="form-control" id="name">
                </div>
                <div class="form-group col-md-6">
                    <label for="lastname">Apellidos <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="lastname">
                </div>
            </div>           
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="tipdoc">Tipo de documento <span class="field_required">*</span></label>
                    <select class="form-control" required id="tipdoc">
                        <option value="">Seleccione</option>
                        <option value="Masculino">Cedula de ciudadania</option>
                        <option value="Femenino">Cedula de extranjeria</option>  
                        <option value="Femenino">Nit</option>          
                        <option value="Femenino">Otro tipo de documento</option>        
                    </select>  
                </div>
                <div class="form-group col-md-6">
                    <label for="numdoc">Número de documento <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="numdoc">
                </div>
            </div>           
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="gender">Género <span class="field_required">*</span></label>
                    <select class="form-control" required id="gender">
                        <option value="">Seleccione</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>                
                    </select>        
                </div>
                <div class="form-group col-md-6">
                    <label for="address">Dirección <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="address">
                </div>      
            </div>
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="city">Ciudad <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="city">
                </div>
                <div class="form-group col-md-6">
                    <label for="telephone">Teléfono</label>
                    <input type="text" class="form-control" id="telephone" >
                </div>
            </div>           
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="mobile">Celular <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="mobile">
                </div>
                <div class="form-group col-md-6">
                    <label for="email">Email <span class="field_required">*</span></label>
                    <input type="text"  required class="form-control" id="email">
                </div>
            </div>  
            <div class="form-group">
                <div class="form-group col-md-6">
                <label for="profile">Perfil <span class="field_required">*</span></label>
                    <select required class="form-control" id="profile">                                               
                        <option value="Cliente">Cliente</option>                
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <input type="hidden" required class="form-control" id="idclient">                     
                </div>               
            </div>  
            
            <button type="button" class="btn btn-primary" id="accept">Aceptar</button>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>

    </div>
    </div>

    <!-- Datatables-->
    <table id="articlesDT" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>     
                <th>Apellido</th>
                <th>Número de documento</th>
                <th>Celular</th>               
                <th>Email</th>
                <th>Acción</th>
                <th>Estado</th>
    
            </tr>
        </thead>        
        <tbody id="tBodyClient">
                <?php 
                    $client = new clientDao();
                    $res=json_decode($client->searchUserAll());
                    echo $res->option;
                ?>                 
        </tbody> 
    </table>
</div>
<script src="../Resources/js/client.js"></script>
<?php include 'Template/footer.php';?>

