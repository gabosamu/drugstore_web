<?php    
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Category.php");
    class categoryDao{
      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta categoría de artículos a través de procedimientos almacenados    
        //  * @author Iván Jojoa
        //  * @date 28/01/2019
           
        public function insert(Category $category){           
           try{           

                $stmt =$this->conexion ->prepare("CALL searchCategoryByName (?);");
                $stmt->bindParam("1", $category->name, PDO::PARAM_STR, 4000);                  
                    
                $stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt =$this->conexion ->prepare("CALL insertCategory (?, ?, ?);");
                    $stmt->bindParam("1", $category->name, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $category->created_at, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("3", $category->is_active, PDO::PARAM_STR, 4000);                 
                        
                    $stmt->execute();
                    return true; 
                }                               
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }           
        }

        public function searchCategoryAll()
        {
            try{                      

                $stmt =$this->conexion->prepare("CALL searchCategoryAll();");
                $stmt->execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .            
                    "<td>" . $row["name"] . "</td>" .
                    "<td>" . $row["created_at"] . "</td>" . 
                    
                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#updateVeh' 
                    onclick=\"$.updateCategory('" . $row["id"] . "','" . $row["name"]  . 
                    "','".$row["created_at"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["is_active"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledCategory('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledCategory('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledCategory('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledCategory('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);

            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }  

         //  * @description Metodo que obtiene todas las categorias
        //  * @author Orlando Mavisoy
        //  * @date 26/01/2019
        
        public function allCategory(){
            try{               

                $stmt =$this->conexion->prepare("CALL searchAllCategory();");                                
                $stmt->execute();                
                $data = "";
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .= "<option value='" . $row['id'] . "'>" .$row['name'].  "</option>";                   
                }  
                $out["option"]=$data;        
                return json_encode($out);              
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }   
        //  * @description Metodo que actualiza el estado del categoría
        //  * @author Iván Jojoa
        //  * @date 30/01/2019



            public function statusCategory(Category $category){
            try{
                             

                $stmt =$this->conexion ->prepare("CALL updateStatusCategory (?, ?);");
                $stmt->bindParam("1", $category->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $category->is_active, PDO::PARAM_STR, 4000); 
              
                $stmt->execute();   
                var_dump($category);                  
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        
        }
        //  * @description Metodo que actualiza una categoría
        //  * @author Iván Jojoa
        //  * @date 30/01/2019
        
        public function update(Category $category){           
            try{                
             
                $stmt = $this->conexion ->prepare("CALL updateCategory (?, ?);");
                $stmt->bindParam("1", $category->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $category->name, PDO::PARAM_STR, 4000);                            
                         
                $stmt->execute();
                return true;                                            
             }catch(Exception $e){
                 die('Error: '. $e->getMessage());               
             }finally{
                $this->conexion = null;
             }     
        }
    }
?>          