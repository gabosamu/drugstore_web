<?php    
    require_once("../Configuration/Connection/Connection.php");    
    require_once("../Model/Entities/Unity.php");
    class unityDao{      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta unidades a través de procedimientos almacenados    
        //  * @author Iván Jojoa
        //  * @date 19/01/2019
           
        public function insert(Unity $unity){           
           try{ 
                $stmt = $this->conexion->prepare("CALL searchUnity (?);");
				$stmt->bindParam("1", $unity->name, PDO::PARAM_STR, 4000);					
					
                $stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt = $this->conexion->prepare("CALL insertUnity (?, ?, ?);");
                    $stmt->bindParam("1", $unity->name, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $unity->create_at, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("3", $unity->status, PDO::PARAM_STR, 4000); 				
                        
                    $stmt->execute();
                    return true; 
                }            					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
				$this->conexion = null;
			}           
        }

        //  * @description Metodo que obtiene todas las unidades y los carga en select
        //  * @author Orlando Mavisoy
        //  * @date 19/01/2019
        
        public function allUnity()
        {
            try{
               
                $stmt =$this->conexion->prepare("CALL searchAllUnity();");                                
                $stmt->execute();                
                $data = "";
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .= "<option value='" . $row['id'] . "'>" .$row['name'].  "</option>";                   
                }  
                $out["option"]=$data;        
                return json_encode($out);   
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }               
        }

        //  * @description Metodo que carga la tabla con datos de unity
        //  * @author Orlando Mavisoy
        //  * @date 19/01/2019
     
        public function searchUnityAll()
        {
            try{
                $stmt = $this->conexion->prepare("CALL searchUnityAll();");
                $stmt->execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .            
                    "<td>" . $row["name"] . "</td>" .
                    "<td>" . $row["create_at"] . "</td>" . 
                    
                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#updateVeh' 
                    onclick=\"$.updateUnity('" . $row["id"] . "','" . $row["name"]  . 
                    "','".$row["create_at"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["status"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledUnity('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledUnity('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledUnity('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledUnity('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);

            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        }  

            public function statusUnity(Unity $unity){
            try{                                     

                $stmt =$this->conexion->prepare("CALL updateStatusUnity (?, ?);");
                $stmt->bindParam("1", $unity->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $unity->status, PDO::PARAM_STR, 4000); 
              
                $stmt->execute();   
                var_dump($unity);                  
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        
        }

        //  * @description Metodo que actualiza una unidad 
        //  * @author Iván Jojoa
        //  * @date 01/02/2019
        
        public function update(Unity $unity){           
            try{                
                              
                $stmt =$this->conexion->prepare("CALL updateUnity (?, ?);");
                $stmt->bindParam("1", $unity->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $unity->name, PDO::PARAM_STR, 4000);                                 
                         
                $stmt->execute();
                return true;                                            
             }catch(Exception $e){
                 die('Error: '. $e->getMessage());               
             }finally{
                $this->conexion = null;
             }     
        }
    }
?>    