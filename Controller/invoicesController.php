<?php
    require_once("../Model/Dao/invoicesDao.php");  
    require_once("../Model/Entities/Invoices.php");
	
	//  * @description Metodo que recibe y envia parametros del articulo
	//  * @author Iván Jojoa
	//  * @date 17/01/2019
	//  */	 
    $Option = $_POST['option'];

	if($Option=="insert"){		
		$invoices = new invoices();
		
		$invoices->bill = $_POST['bill']; 
		$invoices->create_at = date('Y-m-d H:i:s');
		$invoices->lot= $_POST['lot']; 
        $invoices->quantity = $_POST['quantity'];        
		$invoices->costPrice= $_POST['costPrice'];
		$invoices->salePrice = $_POST['salePrice'];
		$invoices->iva = $_POST['iva'];
		$invoices->expirationDate = $_POST['expirationDate'];	
		$invoices->totalPriceCost = $_POST['totalPriceCost'];
		$invoices->totalPriceSale = $_POST['totalPriceSale'];
		$invoices->is_active = 1;	
		$invoices->typeInvoices = 1;		
		$invoices->idprov = $_POST['nameProvider'];
		$invoices->idunity= $_POST['unity'];
		$invoices->idlab= $_POST['laboratory'];
		$invoices->idart= $_POST['articulo'];

		$invoicesDao = new invoicesDao();  
		
		$sql =$invoicesDao->insert($invoices);	
		echo $sql;		

		
	}
?>