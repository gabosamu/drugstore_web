<?php
    class Article{
        public $id;
        public $nameArticle;
        public $referen;
        public $invimaReg;
        public $create_at;
        public $is_active;
        public $idCat;

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }

        /**
         * Get the value of nameArticle
         */ 
        public function getNameArticle()
        {
                return $this->nameArticle;
        }

        /**
         * Set the value of nameArticle
         *
         * @return  self
         */ 
        public function setNameArticle($nameArticle)
        {
                $this->nameArticle = $nameArticle;

                return $this;
        }

        /**
         * Get the value of reference
         */ 
        public function getReferen()
        {
                return $this->referen;
        }

        /**
         * Set the value of reference
         *
         * @return  self
         */ 
        public function setReferen($referen)
        {
                $this->referen = $referen;

                return $this;
        }

        /**
         * Get the value of invimaReg
         */ 
        public function getInvimaReg()
        {
                return $this->invimaReg;
        }

        /**
         * Set the value of invimaReg
         *
         * @return  self
         */ 
        public function setInvimaReg($invimaReg)
        {
                $this->invimaReg = $invimaReg;

                return $this;
        }

        /**
         * Get the value of create_at
         */ 
        public function getCreate_at()
        {
                return $this->create_at;
        }

        /**
         * Set the value of create_at
         *
         * @return  self
         */ 
        public function setCreate_at($create_at)
        {
                $this->create_at = $create_at;

                return $this;
        }

        /**
         * Get the value of is_active
         */ 
        public function getIs_active()
        {
                return $this->is_active;
        }

        /**
         * Set the value of is_active
         *
         * @return  self
         */ 
        public function setIs_active($is_active)
        {
                $this->is_active = $is_active;

                return $this;
        }

        /**
         * Get the value of idCat
         */ 
        public function getIdCat()
        {
                return $this->idCat;
        }

        /**
         * Set the value of idCat
         *
         * @return  self
         */ 
        public function setIdCat($idCat)
        {
                $this->idCat = $idCat;

                return $this;
        }
    }
?>