-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-02-2019 a las 19:58:37
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_drugstore`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE PROCEDURE `buscarUsuarioLogin` (`user` VARCHAR(50), `pass` VARCHAR(50))  BEGIN SELECT * FROM user WHERE username=user AND password = pass;END$$

CREATE PROCEDURE `insertArticle` (`nameArticle` VARCHAR(70), `reference` VARCHAR(50), `invimaReg` VARCHAR(50), `create_at` DATE, `is_active` BIT, `idCat` INT(11))  BEGIN INSERT INTO article (nameArticle,reference,invimaReg,create_at,is_active,idCat) 
VALUES (nameArticle,reference,invimaReg,create_at,is_active,idCat);END$$

CREATE PROCEDURE `insertCategory` (`name` VARCHAR(30), `create_at` DATETIME, `is_active` BIT)  BEGIN INSERT INTO category(name,create_at,is_active) 
VALUES (name,now(),is_active);END$$

CREATE PROCEDURE `insertInvoices` (`bill` VARCHAR(20), `creation_at` DATETIME, `lot` VARCHAR(20), `quantity` INT(20), `costPrice` DOUBLE, `salePrice` DOUBLE, `iva` DOUBLE, `expirationDate` DATE, `totalPriceCost` DOUBLE, `totalPriceSale` DOUBLE, `is_active` BIT(1), `typeInvoices` BIT(1), `_idProv` INT(11), `_idUnity` INT(11), `_idLab` INT(11), `_idArt` INT(11))  BEGIN INSERT INTO ARTICLE (bill,creation_at, lot,quantity,
	costPrice,salePrice,iva,expirationDate,totalPriceCost,totalPriceSale,is_active, typeInvoices,
	idProv, idUnity, idLab, idArt) VALUES (bill, now(), lot,quantity,
	costPrice,salePrice,iva,expirationDate,totalPriceCost,totalPriceSale,is_active, typeInvoices,
	_idProv, _idUnity, _idLab, _idArt);
	END$$

CREATE PROCEDURE `insertLaboratory` (`name` VARCHAR(50), `create_at` DATETIME, `status` BIT)  BEGIN INSERT INTO laboratory(name,create_at,status) 
VALUES (name,now(),status);END$$

CREATE PROCEDURE `insertProvider` (`name` VARCHAR(100), `nit` VARCHAR(45), `address` VARCHAR(100), `city` VARCHAR(50), `phone` INT(20), `cellphone` INT(20), `email` VARCHAR(50), `created_at` DATE, `status` BIT)  BEGIN INSERT INTO provider (name,nit,address,city,phone,cellphone,email,created_at,status) VALUES (name, nit, address, city, phone, cellphone,email, created_at, status);END$$

CREATE PROCEDURE `insertUnity` (`name` VARCHAR(30), `create_at` DATETIME, `status` BIT)  BEGIN INSERT INTO unity(name,create_at,status) 
VALUES (name,now(),status);END$$

CREATE PROCEDURE `insertUser` (`name` VARCHAR(50), `lastname` VARCHAR(50), `tipdoc` VARCHAR(30), `numdoc` INT(20), `gender` VARCHAR(10), `address` VARCHAR(50), `city` VARCHAR(50), `telephone` INT(20), `mobile` INT(20), `email` VARCHAR(255), `username` VARCHAR(50), `password` VARCHAR(60), `created_at` DATETIME, `status` BIT)  BEGIN INSERT INTO user(name,lastname,tipdoc,numdoc,gender,address,city,telephone,mobile,email,username,password,created_at,status) 
VALUES (name,lastname,tipdoc,numdoc,gender,address,city,telephone,mobile,email,username,password,now(),status);END$$

CREATE PROCEDURE `searchAllCategory` ()  BEGIN SELECT id, image, name, created_at, is_active FROM Category where is_active = 1 order by name asc;
END$$

CREATE PROCEDURE `searchAllUnity` ()  BEGIN SELECT id, name, create_at, status FROM unity where status = 1 order by name asc;
END$$

CREATE PROCEDURE `searchArticleByName` ()  BEGIN SELECT id, nameArticle, reference FROM article where is_active = 1 order by nameArticle asc;
END$$

CREATE PROCEDURE `searchArticleByReference` (`_referen` VARCHAR(50))  BEGIN SELECT * FROM article WHERE referen=_referen;
END$$

CREATE  PROCEDURE `searchCategoryAll` ()  BEGIN SELECT * FROM category;
END$$

CREATE PROCEDURE `searchCategoryByName` (`_name` VARCHAR(30))  BEGIN SELECT id, name, create_at, is_active FROM category where name=_name;
END$$

CREATE PROCEDURE `searchInvoicesAll` ()  BEGIN SELECT * FROM invoices as I JOIN article as A ON I.idArt = A.id JOIN 
category as G ON G.id = A.idCat JOIN provider as B ON I.idProv = B.idprov join unity as C 
ON I.idunity = C.id JOIN laboratory as D ON I.idlab = D.id;
END$$

CREATE  PROCEDURE `searchLaboratory` ()  BEGIN SELECT id, name, create_at, status FROM laboratory where status = 1 order by name asc;
END$$

CREATE PROCEDURE `searchLaboratoryAll` ()  BEGIN SELECT * FROM laboratory;
END$$

CREATE  PROCEDURE `searchLaboratoryByName` (`_name` VARCHAR(50))  BEGIN SELECT * FROM laboratory WHERE name=_name;
END$$

CREATE PROCEDURE `searchProvider` (`_name` VARCHAR(50), `_nit` VARCHAR(50))  BEGIN SELECT * FROM provider WHERE name=_name OR nit = _nit;
END$$

CREATE PROCEDURE `searchProviderAll` ()  BEGIN SELECT * FROM provider;
END$$

CREATE PROCEDURE `searchProviderByName` ()  BEGIN SELECT idprov, name, nit FROM provider where status = 1 order by name asc;
END$$

CREATE PROCEDURE `searchUnity` (`_name` VARCHAR(30))  BEGIN SELECT id, name, create_at, status FROM unity where name=_name;
END$$

CREATE  PROCEDURE `searchUnityAll` ()  BEGIN SELECT * FROM unity;
END$$

CREATE PROCEDURE `searchUnityByName` (`_name` VARCHAR(30))  BEGIN SELECT * FROM unity WHERE name=_name;
END$$

CREATE PROCEDURE `searchUser` (`_numdoc` INT(20))  BEGIN SELECT * FROM user WHERE numdoc=_numdoc; END$$

CREATE PROCEDURE `searchUserAll` ()  BEGIN SELECT * FROM user;
END$$

CREATE PROCEDURE `updateAllUser` (`_id` INT(11), `_name` VARCHAR(50), `_lastname` VARCHAR(50), `_tipdoc` VARCHAR(30), `_numdoc` INT(20), `_gender` VARCHAR(10), `_address` VARCHAR(50), `_city` VARCHAR(50), `_telephone` INT(20), `_mobile` INT(20), `_email` VARCHAR(50), `_username` VARCHAR(50), `_password` VARCHAR(50), `_created_at` DATETIME)  BEGIN UPDATE user SET name=_name,lastname=_lastname,tipdoc=_tipdoc,numdoc=_numdoc,gender=_gender,address=_address,city=_city, telephone=_telephone, mobile=_mobile, email = _email, username = _username, password = _password, created_at = now() WHERE id=_id;END$$

CREATE PROCEDURE `updateArticle` (`_id` INT(11), `nameArticle` VARCHAR(70), `referen` VARCHAR(50), `invimaReg` VARCHAR(50), `create_at` DATE, `is_active` BIT, `idCat` INT(20))  BEGIN UPDATE article SET nameArticle=nameArticle,referen=refere,invimaReg=invimaReg, create_at=create_at,is_active=is_active, idCat=idCat WHERE id=_id;END$$

CREATE PROCEDURE `updateCategory` (`_id` INT(11), `name` VARCHAR(50), `create_at` DATE, `is_active` BIT)  BEGIN UPDATE category SET name=name, create_at=create_at,is_active=is_active WHERE id=_id;END$$

CREATE PROCEDURE `updateLaboratory` (`_id` INT(11), `_name` VARCHAR(30))  BEGIN UPDATE laboratory SET name=_name, create_at=now()  WHERE id=_id;END$$

CREATE  PROCEDURE `updateProvider` (`_idprov` INT(11), `name` VARCHAR(100), `nit` VARCHAR(45), `address` VARCHAR(100), `city` VARCHAR(50), `phone` INT(20), `cellphone` INT(20), `email` VARCHAR(50), `created_at` DATE, `status` BIT)  BEGIN UPDATE provider SET name=name,nit=nit,address=address,city=city,phone=phone,cellphone=cellphone,email=email, created_at=created_at,status=status WHERE idprov=_idprov;END$$

CREATE PROCEDURE `updateStatusArticle` (`_id` INT(11), `_is_active` BIT(1))  BEGIN UPDATE article set is_active=_is_active WHERE id=_id;
end$$

CREATE PROCEDURE `updateStatusCategory` (`_id` INT(11), `_is_active` BIT(1))  BEGIN UPDATE category set is_active=_is_active WHERE id=_id;
end$$

CREATE PROCEDURE `updateStatusLaboratory` (`_id` INT(11), `_status` BIT(1))  BEGIN UPDATE laboratory set status=_status WHERE id=_id;
end$$

CREATE  PROCEDURE `updateStatusProvider` (`_idprov` INT(11), `_status` BIT(1))  BEGIN UPDATE provider set status=_status WHERE idprov=_idprov;
end$$

CREATE PROCEDURE `updateStatusUnity` (`_id` INT(11), `_status` BIT(1))  BEGIN UPDATE unity set status=_status WHERE id=_id;
end$$

CREATE PROCEDURE `updateUnity` (`_id` INT(11), `name` VARCHAR(30), `create_at` DATE, `status` BIT)  BEGIN UPDATE unity SET name=name, create_at=create_at,status=status WHERE id=_id;END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `nameArticle` varchar(70) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `invimaReg` varchar(50) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `idCat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `box`
--

CREATE TABLE `box` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `short` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `kind` int(11) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `idInv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `bill` varchar(20) DEFAULT NULL,
  `creation_at` datetime DEFAULT NULL,
  `lot` varchar(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `costPrice` double DEFAULT NULL,
  `salePrice` double DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `expirationDate` date DEFAULT NULL,
  `totalPriceCost` double DEFAULT NULL,
  `totalPriceSale` double DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `typeInvoices` bit(1) DEFAULT NULL,
  `idProv` int(11) NOT NULL,
  `idUnity` int(11) NOT NULL,
  `idLab` int(11) NOT NULL,
  `idArt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratory`
--

CREATE TABLE `laboratory` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `status` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operation`
--

CREATE TABLE `operation` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `q` float DEFAULT NULL,
  `operation_type_id` int(11) DEFAULT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operation_type`
--

CREATE TABLE `operation_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `address1` varchar(50) DEFAULT NULL,
  `address2` varchar(50) DEFAULT NULL,
  `phone1` varchar(50) DEFAULT NULL,
  `phone2` varchar(50) DEFAULT NULL,
  `email1` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `kind` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `inventary_min` int(11) DEFAULT '10',
  `price_in` double DEFAULT NULL,
  `price_out` double DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `presentation` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provider`
--

CREATE TABLE `provider` (
  `idprov` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `nit` varchar(45) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `phone` int(20) DEFAULT NULL,
  `cellphone` int(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `status` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sell`
--

CREATE TABLE `sell` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `operation_type_id` int(11) DEFAULT '2',
  `box_id` int(11) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `cash` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unity`
--

CREATE TABLE `unity` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `status` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `tipdoc` varchar(30) DEFAULT NULL,
  `numdoc` int(20) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `telephone` int(20) DEFAULT NULL,
  `mobile` int(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `tipdoc`, `numdoc`, `gender`, `address`, `city`, `telephone`, `mobile`, `email`, `username`, `password`, `created_at`, `status`) VALUES
(1, 'juan', 'parado', '', 12, 'Femenino', 'ads', 'Bogota', 23, 232, 'ads', '', '', '2019-02-02 15:58:20', NULL),
(2, 'Isabel', 'Guerrero', 'isabel', 0, '1234', 'imagen', '1', 1, 2147483647, NULL, NULL, NULL, NULL, NULL),
(3, 'ivan', '', '', 12, '', '', '', 0, 0, '', 'ivan', '1234', '0000-00-00 00:00:00', b'1'),
(4, 'andres', '', '', 320, '', '', '', 0, 0, '', '', '', '2019-02-04 22:44:19', b'1'),
(5, 'erika', '', '', 890, '', '', '', 0, 0, '', 'erika', '1234', '0000-00-00 00:00:00', b'1'),
(6, 'juan', '', '', 23, '', '', '', 0, 0, '', 'juan', '1234', '0000-00-00 00:00:00', b'1'),
(7, 'jose', '', '', 34, '', '', '', 0, 0, '', 'jose', '1234', '0000-00-00 00:00:00', b'1'),
(8, 'pastor', '', '', 33, '', '', '', 0, 0, '', NULL, NULL, '0000-00-00 00:00:00', b'1'),
(9, 'maria', '', '', 3, '', '', '', 0, 0, '', NULL, NULL, '0000-00-00 00:00:00', b'1'),
(10, 'metro', 'metro', '', 2147483647, '', '', '', 0, 0, '', 'metro', '0000', '0000-00-00 00:00:00', b'1'),
(11, 'asdfd', 'asdfasd', '', 324132123, '', '', '', 0, 0, '', 'as', '11', '0000-00-00 00:00:00', b'1'),
(12, 'prueba10', 'prueba11', 'Masculino', 3223, 'Masculino', 'dad', 'dafas', 0, 3232, 'adasd', 'prueba', '0000', '0000-00-00 00:00:00', b'1'),
(13, 'preuba13', 'df', 'Femenino', 2323, 'Masculino', 'da', 'dsf', 32, 232, 'df', 'OO', '11', '2019-01-29 13:10:29', b'1'),
(14, 'duvan', 'guerrero', 'Masculino', 80807034, 'Masculino', 'la laguna', 'ppasto', 3, 23, '232', '', '', '2019-02-02 15:00:57', b'1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCat_idx` (`idCat`);

--
-- Indices de la tabla `box`
--
ALTER TABLE `box`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `short` (`short`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idInv_idx` (`idInv`);

--
-- Indices de la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idArt_idx` (`idArt`),
  ADD KEY `idProv` (`idProv`),
  ADD KEY `idUnity` (`idUnity`),
  ADD KEY `idLab` (`idLab`);

--
-- Indices de la tabla `laboratory`
--
ALTER TABLE `laboratory`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `operation_type_id` (`operation_type_id`),
  ADD KEY `sell_id` (`sell_id`);

--
-- Indices de la tabla `operation_type`
--
ALTER TABLE `operation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`idprov`);

--
-- Indices de la tabla `sell`
--
ALTER TABLE `sell`
  ADD PRIMARY KEY (`id`),
  ADD KEY `box_id` (`box_id`),
  ADD KEY `operation_type_id` (`operation_type_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `person_id` (`person_id`);

--
-- Indices de la tabla `unity`
--
ALTER TABLE `unity`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `box`
--
ALTER TABLE `box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `laboratory`
--
ALTER TABLE `laboratory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `operation`
--
ALTER TABLE `operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `operation_type`
--
ALTER TABLE `operation_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `provider`
--
ALTER TABLE `provider`
  MODIFY `idprov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `sell`
--
ALTER TABLE `sell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `unity`
--
ALTER TABLE `unity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `idCat` FOREIGN KEY (`idCat`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `idInv` FOREIGN KEY (`idInv`) REFERENCES `invoices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `idArt` FOREIGN KEY (`idArt`) REFERENCES `article` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idLab` FOREIGN KEY (`idLab`) REFERENCES `laboratory` (`id`),
  ADD CONSTRAINT `idProv` FOREIGN KEY (`idProv`) REFERENCES `provider` (`idprov`),
  ADD CONSTRAINT `idUnity` FOREIGN KEY (`idUnity`) REFERENCES `unity` (`id`);

--
-- Filtros para la tabla `operation`
--
ALTER TABLE `operation`
  ADD CONSTRAINT `operation_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `operation_ibfk_2` FOREIGN KEY (`operation_type_id`) REFERENCES `operation_type` (`id`),
  ADD CONSTRAINT `operation_ibfk_3` FOREIGN KEY (`sell_id`) REFERENCES `sell` (`id`);

--
-- Filtros para la tabla `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `sell`
--
ALTER TABLE `sell`
  ADD CONSTRAINT `sell_ibfk_1` FOREIGN KEY (`box_id`) REFERENCES `box` (`id`),
  ADD CONSTRAINT `sell_ibfk_2` FOREIGN KEY (`operation_type_id`) REFERENCES `operation_type` (`id`),
  ADD CONSTRAINT `sell_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `sell_ibfk_4` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
