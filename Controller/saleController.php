<?php
	require_once("../Model/Dao/saleDao.php");  
	require_once("../Model/Entities/Sale.php");
	
	//  * @description Metodo que envia recibe y envia parametros de la venta	
	//  * @author Iván Jojoa
	//  * @date 11/02/2019	
	     
	$Option = $_POST['option'];	
	if($Option=="insert"){
		$sale = new Sale();
		
		$sale->idClient = $_POST['idClient'];
		$sale->nameAdmin =	$_POST['nameAdmin'];	
        $sale->numberInvoice =$_POST['numberInvoice'];
        $sale->idInvoice = $_POST['idInvoice'];	
        $sale->cost=  $_POST['cost'];
        $sale->quantity = $_POST['quantity'];
        $sale->discount = $_POST['discount'];
        $sale->expiration = $_POST['expiration'];
        $sale->total = $_POST['total'];       
        $sale->create_at = "";
		$sale->status = 1;

		$saleDao = new saleDao();
		$sql =$saleDao->insert($sale);	
		echo $sql;		
	}


	//  * @description Metodo que recibe los datos del factura editar el estado del factura	
	//  * @author Iván Jojoa
	//  * @date 13/02/2019	
	     
	if($Option=="statusSale"){
		$sale = new Sale();
		$sale->id =  $_POST['id'];
		$sale->status =  $_POST['status'];		

		$saleDao = new saleDao();
		$sql = $saleDao->statusSale($sale);	
		echo $sql;		
	}	

	//  * @description Metodo que recibe los datos del factura para actualizar	
	//  * @author Iván Jojoa
	//  * @date 13/02/2019	

	if($Option=="update"){
		$sale = new Sale();

		$sale->id = $_POST['id'];
		$sale->idClient = $_POST['idClient'];
		$sale->nameAdmin = $_POST['nameAdmin'];
		$sale->numberInvoice =	$_POST['numberInvoice'];	
        $sale->idInvoice =$_POST['idInvoice'];
        $sale->cost = $_POST['cost'];	
        $sale->quantity=  $_POST['quantity'];
        $sale->discount = $_POST['discount'];
        $sale->expiration = date('Y-m-d H:i:s');
        $sale->total = $_POST['total'];


		$saleDao = new saleDao();
		$sql =$saleDao->update($sale);	
		echo $sql;		
	}
?>