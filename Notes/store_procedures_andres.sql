DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarproducto`(`idpdr` INT, `imagepdr` VARCHAR(50), `barcodepdr` VARCHAR(50), `namepdr` VARCHAR(50), `descriptionpdr` TEXT, `inventary_minpdr` INT, `price_inpdr` FLOAT, `price_outpdr` FLOAT, `unitpdr` VARCHAR(255), `presentationpdr` VARCHAR(255), `user_idpdr` INT, `category_idpdr` INT, `created_atpdr` DATETIME, `is_activepdr` TINYINT)
BEGIN UPDATE product SET image=imagepdr,barcode=barcodepdr,name=namepdr,description=descriptionpdr,inventary_min=inventary_minpdr,price_in=price_inpdr,price_out=price_outpdr,
unit=unitpdr,presentation=presentationpdr,user_id=user_idpdr,category_id=category_idpdr,created_at=created_atpdr,is_active=is_activepdr WHERE id=idpdr;END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarusuario`(`idusr` INT, `nameusr` VARCHAR(50), `lstname` VARCHAR(50), `usrname` VARCHAR(50), `usrmail` VARCHAR(255), `usrpassword` VARCHAR(60), `usrimg` VARCHAR(255), `usr_active` TINYINT, `usr_admin` TINYINT, `usrcreated` DATETIME)
BEGIN UPDATE user SET name=nameusr,lastname=lstname,username=usrname,email=usrmail,password=usrpassword,image=usrimg,is_active=usr_active,is_admin=usr_admin,created_at=usrcreated WHERE id=idusr;END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarproducto`(`idpdr` INT)
BEGIN SELECT * FROM product WHERE id=idpdr;END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarusuario`(`idusr` INT)
BEGIN SELECT * FROM user WHERE id=idusr;END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarusuario`(`idusr` INT)
BEGIN DELETE FROM user WHERE id=idusr;end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarproducto`(`idpdr` INT)
BEGIN DELETE FROM product WHERE id=idpdr;END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `incertarproducto`(`idpdr` INT, `imagepdr` VARCHAR(50), `barcodepdr` VARCHAR(50), `namepdr` VARCHAR(50), `descriptionpdr` TEXT, `inventary_minpdr` INT, `price_inpdr` FLOAT, `price_outpdr` FLOAT, `unitpdr` VARCHAR(255), `presentationpdr` VARCHAR(255), `user_idpdr` INT, `category_idpdr` INT, `created_atpdr` DATETIME, `is_activepdr` TINYINT)
BEGIN INSERT INTO product (id,image,barcode,name,description,inventary_min,price_in,price_out,unit,presentation,user_id,category_id,created_at,is_active) VALUES (idpdr, imagepdr, barcodepdr, namepdr, descriptionpdr, inventary_minpdr,price_inpdr, price_outpdr , unitpdr, presentationpdr, user_idpdr, category_idpdr, created_atpdr, is_activepdr);END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `incertarusuario`(`idusr` INT, `nameusr` VARCHAR(50), `lstname` VARCHAR(50), `usrname` VARCHAR(50), `usrmail` VARCHAR(255), `usrpassword` VARCHAR(60), `usrimg` VARCHAR(255), `usr_active` TINYINT, `usr_admin` TINYINT, `usrcreated` DATETIME)
BEGIN INSERT INTO user(id,name,lastname,username,email,password,image,is_active,is_admin,created_at) VALUES (idusr,nameusr,lstname,usrname,usrmail,usrpassword,usrimg,usr_active,usr_admin,usrcreated);end$$
DELIMITER ;
