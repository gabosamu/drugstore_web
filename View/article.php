<?php 
    require_once 'Template/header.php';
    require_once '../Model/Dao/articleDao.php';
    require_once '../Model/Dao/categoryDao.php';
?>
  
<div class="container">
  <h1>Articulos</h1>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalArticle"> + </button> 
    <!-- Modal -->
    <div id="modalArticle" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Artículos</h4>
        </div>
        <div class="modal-body">
        <form id="form">
            <div class="form-group">
                <label for="nameArticle">Nombre de Artículo <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="nameArticle">
            </div>
            <div class="form-group">
                <label for="referen">Referencia <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="referen">
            </div>
            <div class="form-group">
                <label for="invimaReg">Registro Invima <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="invimaReg" >
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="idCat">Categoría de artículos</label>
                    <select class="form-control" id="idCat">                              
                        <option value="" selected="selected">Seleccione</option>
                        <?php 
                            $category = new categoryDao();
                            $res=json_decode($category->allCategory());
                            echo $res->option;
                        ?>                                 
                    </select>         
                </div>                
            </div>
            <div class="form-group">               
                <input type="hidden" class="form-control" id="idart">
            </div>           
            <button type="button" class="btn btn-danger" id="accept">Aceptar</button>
        </form>
        </div>
        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

    <!-- Datatables-->
    <table id="datatables" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>     
                <th>Referencia</th>               
                <th>Categoría de Artículos</th> 
                <th>Fecha</th>
                <th>Acción</th>
                <th>Estado</th>
            </tr>
        </thead>        
        <tbody id="tBodyArticle">
                <?php 
                    $article = new articleDao();
                    $res=json_decode($article->searchArticleAll());
                    echo $res->option;
                ?>  
               
        </tbody> 
    </table>
</div>
<script src="../Resources/js/article.js"></script>
<?php include 'Template/footer.php';?>