<?php
	require_once("../Model/Dao/unityDao.php");  
	require_once("../Model/Entities/Unity.php");
	
	//  * @description Metodo que recibe los datos del unidades de medida para ser almacenados en bd	
	//  * @author Iván Jojoa
	//  * @date 19/01/2019	
	 
	$Option = $_POST['option'];	
	if($Option=="insert"){
		$unity = new Unity();
		
		$unity->name = $_POST['name'];
		$unity->create_at =	date('Y-m-d H:i:s');
		$unity->status = 1; 

		$unityDao = new unityDao();
		$sql =$unityDao->insert($unity);	
		echo $sql;		
	}
	//  * @description Metodo que recibe los datos del unidad editar el estado de unidad
	//  * @author Iván Jojoa
	//  * @date 01/02/2019	
	     
	if($Option=="statusUnity"){
		$unity = new Unity();
		$unity->id=  $_POST['id'];
		$unity->status =  $_POST['status'];		

		$unityDao = new unityDao();
		$sql = $unityDao->statusUnity($unity);	
		echo $sql;		
	}	

	//  * @description Metodo que recibe los datos del unidad para actualizar	
	//  * @author Iván Jojoa
	//  * @date 01/02/2019	

	if($Option=="update"){
		$unity = new Unity();

		$unity->id = $_POST['id'];
		$unity->name = $_POST['name']; 	

		$unityDao = new unityDao();
		$sql =$unityDao->update($unity);	
		echo $sql;		
	}



?>
