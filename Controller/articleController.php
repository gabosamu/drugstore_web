<?php
	require_once("../Model/Dao/articleDao.php");  
	require_once("../Model/Entities/Article.php");
	
	//  * @description Metodo que recibe los datos de categoría de artículos para ser almacenados en bd	
	//  * @author Iván Jojoa
	//  * @date 28/01/2019	
	 
	$Option = $_POST['option'];	
	if($Option=="insert"){
		$article = new Article();
		
		$article->nameArticle = $_POST['nameArticle'];
		$article->referen = $_POST['referen'];
		$article->invimaReg = $_POST['invimaReg'];
		$article->create_at = date('Y-m-d H:i:s');	
		$article->is_active = 1; 
		$article->idCat = $_POST['idCat']; 

		$articleDao = new articleDao();
		$sql =$articleDao->insert($article);	
		echo $sql;		
	}

	//  * @description Metodo que recibe los datos del proveedor editar el estado del proveedor	
	//  * @author Iván Jojoa
	//  * @date 01/02/2019	
	     
	if($Option=="statusArticle"){
		$article = new Article();
		$article->id =  $_POST['id'];
		$article->is_active =  $_POST['is_active'];		

		$articleDao = new articleDao();
		$sql = $articleDao->statusArticle($article);	
		echo $sql;		
	}	

	//  * @description Metodo que recibe los datos del proveedor para actualizar	
	//  * @author Iván Jojoa
	//  * @date 01/02/2019	

	if($Option=="update"){
		$article = new Article();
		$article->id = $_POST['id'];
		$article->nameArticle = $_POST['nameArticle'];
		$article->referen = $_POST['referen'];
		$article->invimaReg = $_POST['invimaReg'];
		$article->create_at =	date('Y-m-d H:i:s');	
		$article->idCat = $_POST['idCat']; 

		$articleDao = new articleDao();
		$sql =$articleDao->update($article);	
		echo $sql;		
	}
?>
