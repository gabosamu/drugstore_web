<?php
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Invoices.php");
    class invoicesDao{
      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta facturas   
        //  * @author Iván Jojoa
        //  * @date 17/01/2019

        public function insert(Invoices $invoices){ 
            
           try{                
				
                    $stmt=$this->conexion->prepare("CALL insertInvoices(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?, ?, ?,?);");
                    $stmt->bindParam("1", $invoices->bill, PDO::PARAM_STR, 4000);       
                    $stmt->bindParam("2", $invoices->create_at, PDO::PARAM_STR, 4000);                  
                    $stmt->bindParam("3", $invoices->lot, PDO::PARAM_STR, 4000);                                   
                    $stmt->bindParam("4", $invoices->quantity, PDO::PARAM_STR, 4000);                    				
                    $stmt->bindParam("5", $invoices->costPrice, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("6", $invoices->salePrice, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("7", $invoices->iva, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("8", $invoices->expirationDate, PDO::PARAM_STR, 4000);  
                    $stmt->bindParam("9", $invoices->totalPriceCost, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("10", $invoices->totalPriceSale, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("11", $invoices->is_active, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("12", $invoices->typeInvoices, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("13", $invoices->idprov, PDO::PARAM_STR, 4000);     
                    $stmt->bindParam("14", $invoices->idunity, PDO::PARAM_STR, 4000);  
                    $stmt->bindParam("15", $invoices->idlab, PDO::PARAM_STR, 4000);      
                    $stmt->bindParam("16", $invoices->idart, PDO::PARAM_STR, 4000);              
                    $stmt->execute();
                    return true;                            					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
                $this->conexion = null;
			}           
        }    

        //  * @description Metodo que obtiene todos las facturas
        //  * @author Iván Jojoa
        //  * @date 17/01/2019

        public function searchInvoicesAll()
        {
            try{
              
                $stmt = $this->conexion->prepare("CALL searchInvoicesAll();");
                $stmt->execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .                            
                    "<td>" . $row["nameArticle"] . "</td>" .                                               
                    "<td>" . $row["quantity"] . "</td>" .   
                    "<td>" . $row["name"] . "</td>" .                  
                    "<td>" . $row["costPrice"] . "</td>" .
                    "<td>" . $row["salePrice"] . "</td>" .
                    "<td>" . $row["iva"] . "</td>" .
                    "<td>" . $row["totalPriceCost"] . "</td>" .
                    "<td>" . $row["totalPriceSale"] . "</td>" .              
                    
                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#updateVeh' 
                    onclick=\"$.updateArticle('" . $row["bill"] .  "','" . $row["nameArticle"] . "','" . $row["reference"] . "','" . $row["lot"] . "','" . 
                    $row["invimaRegistration"] . "','" . $row["quantity"] . "','".$row["costPrice"]."','".$row["salePrice"]."','".$row["iva"]."','".$row["expirationDate"].
                    "','".$row["totalPriceCost"]. "','".$row["totalPriceSale"]."','".$row["is_active"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["status"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledArticle('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledArticle('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledArticle('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledArticle('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }           
        }

        //  * @description Metodo que actualiza el estado de la factura
        //  * @author Ivan Jojoa
        //  * @date 12/01/2019
        
        public function statusInvoices($id, $status){
            try{
               
                $stmt =$this->conexion->prepare("CALL updateStatusInvoice (?, ?);");
                $stmt->bindParam("1", $id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $status, PDO::PARAM_STR, 4000);                   
                    
                $stmt->execute();
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    
        } 
    } 
?>