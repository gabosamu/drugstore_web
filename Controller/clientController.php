<?php
    require_once("../Model/Dao/clientDao.php");  
    require_once("../Model/Entities/Client.php");
	
	//  * @description Metodo que recibe y envia parametros del proveedor	
	//  * @author Orlando Mavisoy Guerrero
	//  * @date 09/01/2019
	 
    $Option = $_POST['option'];
	if($Option=="insert"){
		
		$client = new Client();
		$client->name = $_POST['name'];
		$client->lastname = $_POST['lastname'];
        $client->tipdoc = $_POST['tipdoc'];
        $client->numdoc = $_POST['numdoc'];	
        $client->gender= $_POST['gender'];
        $client->address = $_POST['address'];
        $client->city = $_POST['city'];
        $client->telephone = $_POST['telephone'];
        $client->mobile = $_POST['mobile'];
		$client->email= $_POST['email'];
		$client->username = "";
		$client->password = "";
		$client->created_at = date('Y-m-d H:i:s');	
		$client->profile = $_POST['profile'];;
		$client->status = 1;	
		
		$clientDao = new clientDao();  
		
		$sql =$clientDao->insert($client);	
		echo $sql;		
	}
//  * @description Metodo que recibe los datos del cliente editar el estado del cliente
	//  * @author Iván Jojoa
	//  * @date 22/01/2019	
	     
	if($Option=="statusClient"){
		$client = new Client();
		$client->id =  $_POST['id'];
		$client->status =  $_POST['status'];		

		$clientDao = new clientDao();
		$sql = $clientDao->statusClient($client);	
		echo $sql;		
	}	



	//  * @description Metodo que recibe los datos del cliente para actualizar	
	//  * @author Iván Jojoa
	//  * @date 18/01/2019	

	if($Option=="update"){	

		$client = new Client();
		$client->id =  $_POST['id'];
		$client->name = $_POST['name'];
		$client->lastname = $_POST['lastname'];
        $client->tipdoc = $_POST['tipdoc'];
        $client->numdoc = $_POST['numdoc'];	
        $client->gender= $_POST['gender'];
        $client->address = $_POST['address'];
        $client->city = $_POST['city'];
        $client->telephone = $_POST['telephone'];
        $client->mobile = $_POST['mobile'];
		$client->email= $_POST['email'];
		$client->username = "";
		$client->password = "";
		$client->profile= $_POST['profile'];
		$client->created_at = "";		

		$clientDao = new clientDao();
		$sql =$clientDao->update($client);	
		echo $sql;		
	}



?>