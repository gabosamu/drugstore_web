<?php
    class Payment{
        public $id;
        public $invoiceP;
        public $nameClient;
        public $val;
        public $invoiceC;
        public $create_at;
        public $status;

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }

        /**
         * Get the value of invoiceP
         */ 
        public function getInvoiceP()
        {
                return $this->invoiceP;
        }

        /**
         * Set the value of invoiceP
         *
         * @return  self
         */ 
        public function setInvoiceP($invoiceP)
        {
                $this->invoiceP = $invoiceP;

                return $this;
        }

        /**
         * Get the value of name Client
         */ 
        public function getNameClient()
        {
                return $this->nameClient;
        }

        /**
         * Set the value of name Client
         *
         * @return  self
         */ 
        public function setNameClient($nameClient)
        {
                $this->nameClient = $nameClient;

                return $this;
        }

        /**
         * Get the value of val
         */ 
        public function getVal()
        {
                return $this->val;
        }

        /**
         * Set the value of val
         *
         * @return  self
         */ 
        public function setVal($val)
        {
                $this->val = $val;

                return $this;
        }


        /**
         * Get the value of invoiceC
         */ 
        public function getInvoiceC()
        {
                return $this->invoiceC;
        }

        /**
         * Set the value of invoiceC
         *
         * @return  self
         */ 
        public function setInvoiceC($invoiceC)
        {
                $this->invoiceC = $invoiceC;

                return $this;
        }

        /**
         * Get the value of create_at
         */ 
        public function getCreate_at()
        {
                return $this->create_at;
        }

        /**
         * Set the value of create_at
         *
         * @return  self
         */ 
        public function setCreate_at($create_at)
        {
                $this->create_at = $create_at;

                return $this;
        }

        

        /**
         * Get the value of status
         */ 
        public function getStatus()
        {
                return $this->status;
        }

        /**
         * Set the value of idCat
         *
         * @return  self
         */ 
        public function setStatus($status)
        {
                $this->status = $status;

                return $this;
        }
    }
?>