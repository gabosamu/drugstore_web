<?php 
    require_once 'Template/header.php';
    require_once '../Model/Dao/invoicesDao.php';
    require_once '../Model/Dao/articleDao.php';
    require_once '../Model/Dao/providerDao.php';
    require_once '../Model/Dao/unityDao.php';
    require_once '../Model/Dao/laboratoryDao.php';
    require_once '../Model/Dao/categoryDao.php';
?>  
  
<div class="container">
  <h1>Factura de proveedores</h1>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalArticles"> + </button> 
    <!-- Modal -->
    <div id="modalArticles" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Factura de compra</h4>
        </div>
        <div class="modal-body">
        <form>           
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="idProv">Proveedor</label>
                    <select class="form-control" id="idProv">                              
                        <option value="" selected="selected">Seleccione</option>
                        <?php 
                            $provider = new providerDao();
                            $res=json_decode($provider->allProviderByName());
                            echo $res->option;
                        ?>                                 
                    </select>     
                </div>       
                      
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">    
                        <label for="idArt">Nombre articulo</label>                                 
                        <select class="selectpicker"  id="idArt" data-show-subtext="true" data-live-search="true">
                            <option value="" selected="selected">Seleccione</option>                          
                            <?php 
                                $article = new articleDao();
                                $res=json_decode($article->allArticleByName());
                                echo $res->option;
                            ?>                                     
                        </select>               
                </div>           
                <div class="form-group col-md-6">
                    <label for="bill">Nº factura</label>
                    <input type="text" class="form-control" id="bill">
                </div>          
            </div>   
            <div class="form-row">
                <div class="form-group col-md-6">
                        <label for="typeInvoice">Tipo de factura</label>                   
                        <select class="form-control" readonly="readonly" id="typeInvoice">
                            <option value="1" selected="selected">Compra</option>                                       
                        </select>         
                </div>
                <div class="form-group col-md-6">
                <label for="lot">Lote</label>
                <input type="text" class="form-control" id="lot" placeholder="Referencia">
                </div>
            </div>
            <div class="form-row">               
                <div class="form-group col-md-6">
                <label for="idLab">Laboratorio</label>
                <select class="selectpicker" id="idLab" data-show-subtext="true" data-live-search="true">
                <option value="" selected="selected">Seleccione</option>                          
                        <?php 
                            $laboratory = new laboratoryDao();
                            $res=json_decode($laboratory->allLaboratory());
                            echo $res->option;
                        ?>   
                </select>   
                </div>
                <div class="form-group col-md-6">
                    <label for="idCat">Categoría</label>
                    <select class="form-control" id="idCat">                              
                        <option value="" selected="selected">Seleccione</option>
                        <?php 
                            $category = new categoryDao();
                            $res=json_decode($category->allCategory());
                            echo $res->option;
                        ?>                                 
                    </select>     
                </div>                
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                <label for="quantity">Cantidad</label>
                <input type="text" class="form-control priceCost" onkeyup="$.addPriceCost()" id="quantity">
                </div>
                <div class="form-group col-md-6">
                <label for="unity">Unidad</label>
                <select class="selectpicker" id="unity" data-show-subtext="true" data-live-search="true">
                <option value="" selected="selected">Seleccione</option>                          
                        <?php 
                            $unity = new unityDao();
                            $res=json_decode($unity->allUnity());
                            echo $res->option;
                        ?>   
                </select>   
                </div>
            </div>
            <div class="form-row">               
                <div class="form-group col-md-6">
                <label for="costPrice">Precio de costo</label>
                <input type="text" class="form-control priceCost" onkeyup="$.addPriceCost()" id="costPrice" >
                </div>
                <div class="form-group col-md-6">
                <label for="salePrice">Precio de venta</label>
                <input type="text" class="form-control priceSale" onkeyup="$.addPriceSale()" id="salePrice" >
                </div>
            </div>           
            <div class="form-row">
                <div class="form-group col-md-6">
                <label for="iva">IVA</label>
                <input type="text" class="form-control priceCost" onkeyup="$.addPriceCost()" id="iva" placeholder="0.19">
                </div>
                <div class="form-group col-md-6">
                <label for="expirationDate">Fecha vencimiento</label>
                <input type="date" class="form-control" id="expirationDate" placeholder="Ejemplo: dd/mm/yyyy">
                </div>
            </div>             
            <div class="form-row">
                <div class="form-group col-md-6">
                <label for="totalPriceCost">Total precio costo</label>
                <input type="test" readonly="readonly"  class="form-control" id="totalPriceCost" >
                </div>
                <div class="form-group col-md-6">
                <label for="totalPriceSale">Total precio venta</label>
                <input type="text" readonly="readonly" class="form-control" id="totalPriceSale" placeholder="$">
                </div>
            </div> 
            <button type="button" class="btn btn-primary" id="accept">Aceptar</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>

    </div>
    </div>
    <!-- Datatables-->
    <table id="articlesDT" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre articulo</th>      
                <th>Cantidad</th>
                <th>Unidad</th>
                <th>Valor unitario</th>
                <th>Valor venta</th>
                <th>IVA</th>                
                <th>Total P/C</th>
                <th>Total P/V</th>
                <th>Acción</th>           
                <th>Estado</th>                
            </tr>
        </thead>
        <tbody id="tBodyInvoices">           
            <?php 
                $invoices = new invoicesDao();
                $res=json_decode($invoices->searchInvoicesAll());
                echo $res->option;
            ?>  
        </tbody>      
    </table>
</div>
<script src="../Resources/js/invoices.js"></script>
<?php include 'Template/footer.php';?>