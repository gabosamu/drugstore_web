DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_categoria`(`pid` INT)
BEGIN SELECT*from category WHERE id=pid; 
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_categoria`(`pid` INT)
BEGIN DELETE FROM category WHERE id=pid; 
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_categoria`(`pid` INT, `pimage` VARCHAR(255), `pname` VARCHAR(50), `pdescription` TEXT, `pcreated_at` DATETIME, `pcondition` VARCHAR(45))
begin
update category set image=pimage,name=pname, description=pdescription,created_at=pcreated_at, conditio=pcondition WHERE id=pid;
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_person`(`pid` INT)
BEGIN SELECT*from person WHERE id=pid; 
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_person`(`pid` INT)
BEGIN DELETE FROM person WHERE id=pid; 
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_person`(`pid` INT, `pimage` VARCHAR(255), `pname` VARCHAR(255), `pcompany` VARCHAR(50), `paddress1` VARCHAR(50), `paddress2` VARCHAR(50), `pphone1` VARCHAR(50), `pphone2` VARCHAR(50), `pemail1` VARCHAR(50), `pemail2` VARCHAR(50), `pkind` INT, `pcreated_at` DATETIME)
begin
update person set image=pimage,name=pname, lastname=plastname,company=pcompany,address1=paddress1,address2=paddress2,phone1=pphone1,phone2=pphone2,email1=pemail1,email2=pemail2,kind=pkind,created_at=pcreated_at WHERE id=pid;
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ingresar_categoria`(`pid` INT, `pimage` VARCHAR(255), `pname` VARCHAR(50), `pdescription` TEXT, `pcreated_at` DATETIME, `pcondition` VARCHAR(45))
begin insert into category
(id,image,name, description,created_at, conditio)
values(pid, pimage, pname, pdescription, pcreated_at, pcondition);
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ingresar_person`(IN `pid` INT, IN `pimage` VARCHAR(255), IN `pname` VARCHAR(255), IN `plastname` VARCHAR(50), IN `pcompany` VARCHAR(50), IN `paddress1` VARCHAR(50), IN `paddress2` VARCHAR(50), IN `pphone1` VARCHAR(50), IN `pphone2` VARCHAR(50), IN `pemail1` VARCHAR(50), IN `pemail2` VARCHAR(50), IN `pkind` INT, IN `pcreated_at` DATETIME)
begin
insert into person (id,image,name, lastname,company,address1,address2,phone1,phone2,email1,email2,kind,created_at)
values(pid, pimage, pname, plastname, pcompany, paddress1, paddress2, pphone1, pphone2, pemail1,pemail2,pkind,pcreated_at);
end$$
DELIMITER ;
