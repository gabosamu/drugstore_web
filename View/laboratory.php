<?php 
    require_once 'Template/header.php';
    require_once '../Model/Dao/laboratoryDao.php';
?>
  
<div class="container">
  <h1>Laboratorios</h1>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalLaboratory"> + </button> 
    <!-- Modal -->
    <div id="modalLaboratory" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Laboratorios</h4>
                </div>
            <div class="modal-body">
            <form id="form">
                <div class="form-group">
                    <label for="name">Nombre del Laboratorio <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="name">
                </div> 
                <div class="form-group">               
                    <input type="hidden" class="form-control" id="idlab">
                </div>          
                <button type="button" class="btn btn-danger" id="accept">Aceptar</button>
            </form>
          
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    </div>
    <!-- Datatables-->
    <table id="datatables" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>  
                <th>Fecha</th>  
                <th>Acción</th>
                <th>Estado</th>   

            </tr>
        </thead>        
        <tbody id="tBodyLaboratory">
                <?php 
                    $laboratory = new laboratoryDao();
                    $res=json_decode($laboratory->searchLaboratoryAll());
                    echo $res->option;
                ?>                         
        </tbody> 
    </table>
</div>
<script src="../Resources/js/laboratory.js"></script>
<?php include 'Template/footer.php';?>