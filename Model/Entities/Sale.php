<?php
    class Sale{
        public $id;
        public $idClient;
        public $nameAdmin;
        public $numberInvoice;
        public $idInvoice;
        public $cost;
        public $quantity;
        public $discount;
        public $expiration;
        public $total;
        public $create_at;
        public $status;


                /**
         * Get the value of idClient
         */ 
        public function getIdClient()
        {
                return $this->idClient;
        }

        /**
         * Set the value of idClient
         *
         * @return  self
         */ 
        public function setIdClient($idClient)
        {
                $this->idClient = $idClient;

                return $this;
        }
        
        /**
         * Get the value of name
         */ 
        public function getNameAdmin()
        {
                return $this->nameAdmin;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */ 
        public function setNameAdmin($nameAdmin)
        {
                $this->nameAdmin = $nameAdmin;

                return $this;
        }

        /**
         * Get the value of numberInvoice
         */ 
        public function getNumberInvoice()
        {
                return $this->numberInvoice;
        }

        /**
         * Set the value of numberInvoice
         *
         * @return  self
         */ 
        public function setNumberInvoice($numberInvoice)
        {
                $this->numberInvoice = $numberInvoice;

                return $this;
        }

        /**
         * Get the value of idInvoice
         */ 
        public function getIdInvoice()
        {
                return $this->idInvoice;
        }

        /**
         * Set the value of idInvoice
         *
         * @return  self
         */ 
        public function setIdInvoice($idInvoice)
        {
                $this->idInvoice = $idInvoice;

                return $this;
        }

        /**
         * Get the value of cost
         */ 
        public function getCost()
        {
                return $this->cost;
        }

        /**
         * Set the value of cost
         *
         * @return  self
         */ 
        public function setCost($cost)
        {
                $this->cost = $cost;

                return $this;
        }

        /**
         * Get the value of quantity
         */ 
        public function getQuantity()
        {
                return $this->quantity;
        }

        /**
         * Set the value of quantity
         *
         * @return  self
         */ 
        public function setQuantity($quantity)
        {
                $this->quantity = $quantity;

                return $this;
        }

        /**
         * Get the value of discount
         */ 
        public function getDiscount()
        {
                return $this->discount;
        }

        /**
         * Set the value of discount
         *
         * @return  self
         */ 
        public function setDiscount($discount)
        {
                $this->discount = $discount;

                return $this;
        }

        /**
         * Get the value of expiration
         */ 
        public function getExpiration()
        {
                return $this->expiration;
        }

        /**
         * Set the value of expiration
         *
         * @return  self
         */ 
        public function setExpiration($expiration)
        {
                $this->expiration = $expiration;

                return $this;
        }

        /**
         * Get the value of total
         */ 
        public function getTotal()
        {
                return $this->total;
        }

        /**
         * Set the value of total
         *
         * @return  self
         */ 
        public function setTotal($total)
        {
                $this->total = $total;

                return $this;
        }
        /**
         * Get the value of create_at
         */ 
        public function getCreate_at()
        {
                return $this->create_at;
        }

        /**
         * Set the value of create_at
         *
         * @return  self
         */ 
        public function setCreate_at($create_at)
        {
                $this->create_at = $create_at;

                return $this;
        }

        /**
         * Get the value of status
         */ 
        public function getStatus()
        {
                return $this->status;
        }

        /**
         * Set the value of status
         *
         * @return  self
         */ 
        public function setStatus($status)
        {
                $this->status = $status;

                return $this;
        }

      

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }
    }
?>