<?php
	require_once("../Model/Dao/laboratoryDao.php");  
	require_once("../Model/Entities/Laboratory.php");
	
	//  * @description Metodo que envia recibe y envia parametros del laboratorio
	//  * @author Iván Joja
	//  * @date 18/01/2019	
	     
	$Option = $_POST['option'];	
	if($Option=="insert"){
		$laboratory = new Laboratory();
		
		$laboratory->name = $_POST['name'];
		$laboratory->create_at = date('Y-m-d H:i:s');
		$laboratory->status = 1;

		$laboratoryDao = new laboratoryDao();
		$sql =$laboratoryDao->insert($laboratory);	
		echo $sql;

	}
	//  * @description Metodo que recibe los datos del laboratorio editar el estado del laboratorio	
	//  * @author Iván Jojoa
	//  * @date 31/01/2019	
	     
	if($Option=="statusLaboratory"){
		$laboratory = new Laboratory();
		$laboratory->id =  $_POST['id'];
		$laboratory->status =  $_POST['status'];		

		$laboratoryDao = new laboratoryDao();
		$sql = $laboratoryDao->statusLaboratory($laboratory);	
		echo $sql;		
	}	

	//  * @description Metodo que recibe los datos del laboratorio para actualizar	
	//  * @author Iván Jojoa
	//  * @date 31/01/2019	

	if($Option=="update"){
		$laboratory = new Laboratory();

		$laboratory->id = $_POST['id'];
		$laboratory->name = $_POST['name'];        	

		$laboratoryDao = new laboratoryDao();
		$sql = $laboratoryDao->update($laboratory);	
		echo $sql;		
	}
?>	

