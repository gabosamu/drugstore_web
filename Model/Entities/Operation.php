<?php
	class Operation{
		public $id;
		public $product_id;
		public $q;
		public $operation_type_id;
		public $sell_id;
		public $created_at;
		

		public Operation($id, $product_id, $q, $operation_type_id, $sell_id, $created_at)
		{
			$this->id = $id;
			$this->product_id = $product_id;
			$this->q = $q;
			$this->operation_type_id = $operation_type_id;
			$this->sell_id = $sell_id;
			$this->created_at = $created_at;

		}
		public function getId()
		{
		    return $this->id;
		}
		
		public function setId($id)
		{
		    $this->id = $id;
		    return $this;
		}

		public function getProduct_id()
		{
		    return $this->product_id;
		}
		
		public function setProduct_id($product_id)
		{
		    $this->product_id = $product_id;
		    return $this;
		}

		public function getQ()
		{
		    return $this->q;
		}
		
		public function setQ($q)
		{
		    $this->q = $q;
		    return $this;
		}

		public function getOperation_type_id()
		{
		    return $this->Operation_type_id;
		}
		
		public function setOperation_type_id($operation_type_id)
		{
		    $this->operation_type_id = $operation_type_id;
		    return $this;
		}

		public function getSell_id()
		{
		    return $this->sell_id;
		}
		
		public function setSell_id($sell_id)
		{
		    $this->sell_id = $sell_id;
		    return $this;
		}

		public function getCreated_at()
		{
		    return $this->created_at;
		}
		
		public function setCreated_at($created_at)
		{
		    $this->created_at = $created_at;
		    return $this;
		}

		
		

	}
?>