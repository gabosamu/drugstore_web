        /*     
         * @description Valida campos vacios
         * @author Orlando Mavisoy Guerrero
         * @date 08/01/2019
         */ 
        $(document).ready(function(){
          
            $("#form input").change(function() {
                var form = $(this).parents("#form");                
                var check = checkCampos(form);
                console.log(check);
                if(check) {                   
                    $("#accept").prop("disabled", false);
                }
                else {                   
                    $("#accept").prop("disabled", true);
                }
            });

            //Función para comprobar los campos de texto
            function checkCampos(obj) {
                var camposRellenados = true;
                obj.find("input").each(function() {                   
                var $this = $(this);
                        if( $this.val().length <= 0 ) {                           
                            camposRellenados = false;
                            return false;
                        }
                });
                if(camposRellenados == false) {
                    return false;
                }
                else {
                    return true;
                }
            }         
        });
        
        
        
        
        
        