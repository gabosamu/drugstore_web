<?php
    require_once("../Configuration/Connection/Connection.php");      
    require_once("../Model/Entities/Administrator.php");
    class administratorDao{      
        public $conexion;
      
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que inserta proveedores atravez de procedimientos almacenados    
        //  * @author Ivan Jojoa
        //  * @date 10/01/2019
           
        public function insert(Administrator $administrator){ 
            
           try{  
                $stmt =$this->conexion->prepare("CALL searchUser (?);");
				$stmt->bindParam("1", $administrator->numdoc, PDO::PARAM_STR, 4000);
								
				$stmt->execute();
                if($fila = $stmt->fetch(PDO::FETCH_ASSOC))
                {                   
                    return false;
                }else{
                    $stmt=$this->conexion->prepare("CALL insertUser(?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?, ?, ?, ?);");
                    $stmt->bindParam("1", $administrator->name, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $administrator->lastname, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("3", $administrator->tipdoc, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("4", $administrator->numdoc, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("5", $administrator->gender, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("6", $administrator->address, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("7", $administrator->city, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("8", $administrator->telephone, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("9", $administrator->mobile, PDO::PARAM_STR, 4000);					
                    $stmt->bindParam("10",$administrator->email, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("11",$administrator->username, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("12",$administrator->password, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("13",$administrator->created_at, PDO::PARAM_STR, 4000);  
                    $stmt->bindParam("14",$administrator->profile, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("15",$administrator->status, PDO::PARAM_STR, 4000);                         
                    $stmt->execute();
                    return true;                     
                }            					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());               
			}finally{
				$this->conexion = null;
			}           
        }
       
        //  * @description Metodo que obtiene todos los administradores   
        //  * @author Ivan Jojoa
        //  * @date 12/01/2019           

        public function searchUserAll()
        { 
            try{
                $stmt = $this->conexion->prepare("CALL searchUserAll();");
                $stmt->execute();                     
                $data = "";
                        
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){     
                    $data .=
                    "<tr>" .            
                    "<td>" . $row["name"] . "</td>" .
                    "<td>" . $row["lastname"] . "</td>" .
                    "<td>" . $row["numdoc"] . "</td>" .
                    "<td>" . $row["mobile"] . "</td>" .
                    "<td>" . $row["email"] . "</td>" .                   
                    "<td align='center'>" .
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#modalAdmon' 
                    onclick=\"$.updateAdministrator('" . $row["id"] . "','" . $row["name"] . "','" .
                    $row["lastname"] . "','" . $row["tipdoc"] . "','" . $row["numdoc"] . "','" . 
                    $row["gender"] . "','" . $row["address"] . "','" . $row["city"] . 
                    "','".$row["telephone"]."','".$row["mobile"]."','".$row["email"]."','".$row["username"]."','".$row["password"]."','".$row["profile"]."');\">
                    <i class='fa fa-edit'></i> Modificar</button> ".
                    
                    "<td align='center'>" .
                    "";
                    if ($row["status"] === '0') {
                        $data .= '' .
                        "<button type='button' class='btn btn-warning' id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledAdministrator('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button'  style='display: none' class='btn btn-success'  id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledAdministrator('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    } else {
                        $data .= '' .
                        "<button type='button' style='display: none' class='btn btn-warning'  id=\"btnDisabled" . $row["id"] . "\" onclick=\"$.disabledClient('" . $row["id"] . "');\" ><i class='fa fa-wrench'></i> No disponible</button>";
                        $data .= '' .
                        "<button type='button' class='btn btn-primary' id=\"btnEnabled" . $row["id"] . "\" onclick=\"$.enabledAdministrator('" . $row["id"] . "');\" ><i class='fa fa-car'></i> Disponible</button>";
                    }           
                    "</tr>";                  
                    }
                    $out["option"]=$data;        
                    return json_encode($out);
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            }    

        }    
        //  * @description Metodo que actualiza el estado del administrador
        //  * @author Iván Jojoa   

       public function statusAdministrator(Administrator $administrator){
            try{                  

                $stmt = $this->conexion->prepare("CALL updateStatusUser (?, ?);");
                $stmt->bindParam("1", $administrator->id, PDO::PARAM_STR, 4000);
                $stmt->bindParam("2", $administrator->status, PDO::PARAM_STR, 4000); 

                $stmt->execute();  
                             
                return true;
            }catch(Exception $e){
                die('Error: '. $e->getMessage());               
            }finally{
                $this->conexion = null;
            } 
        }

        //  * @description Metodo que actualiza un administrador
        //  * @author Iván Jojoa
        //  * @date 18/01/2019
        
        public function update(Administrator $administrator){           
            try{       
                    $stmt = $this->conexion->prepare("CALL updateAllUser (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");                
                    $stmt->bindParam("1", $administrator->id, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("2", $administrator->name, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("3", $administrator->lastname, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("4", $administrator->tipdoc, PDO::PARAM_STR, 4000); 
                    $stmt->bindParam("5", $administrator->numdoc, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("6", $administrator->gender, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("7", $administrator->address, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("8", $administrator->city, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("9", $administrator->telephone, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("10",$administrator->mobile, PDO::PARAM_STR, 4000);                    
                    $stmt->bindParam("11",$administrator->email, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("12",$administrator->username, PDO::PARAM_STR, 4000);
                    $stmt->bindParam("13",$administrator->password, PDO::PARAM_STR, 4000);                   
                    $stmt->bindParam("14",$administrator->created_at, PDO::PARAM_STR, 4000);   
                    $stmt->bindParam("15",$administrator->profile, PDO::PARAM_STR, 4000);                           
                $stmt->execute();
                return true;                                            
             }catch(Exception $e){  
                 die('Error: '. $e->getMessage());               
             }finally{
                $this->conexion = null;
             }           
         }          
    }
?>