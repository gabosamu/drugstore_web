<?php
	class Operation_type{
		public $id;
		public $name;
		

		public Operation_type($id, $name)
		{
			$this->id = $id;
			$this->name = $name;
			
		}
		public function getId()
		{
		    return $this->id;
		}
		
		public function setId($id)
		{
		    $this->id = $id;
		    return $this;
		}

		public function getName()
		{
		    return $this->name;
		}
		
		public function setName($name)
		{
		    $this->name = $name;
		    return $this;
		}

		
		

	}
?>