/**
         * @deprecated
         * @version 1.0        
         * @description Validar inicio de sesión
         * @author Iván Jojoa
         * @date 28/01/2019
         */ 
        $(document).ready(function(){      
         
            $.extend({
                 /*
                @description Funcion que envia los datos de categoría de artículos para ser almacenados en base de datos
                @author Iván Jojoa
                @date 28/01/2019
                */
                addCategory: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/categoryController.php",               
                        success: function (data) {
                             if((data==="") || (data==="false")){				   	
                                alert("La categoria ya existe");                                
                            }else{
                                alert("Registro exitoso");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }, 

                 /*
                @description actualiza los datos de categoría 
                @author Iván Jojoa
                @date 30/01/2019
                */
                updateCategory: function(id, name){            
                    $('#modalCategory').modal('show');                                   
                    $("#name").val(name);                 
                    $("#idcat").val(id);                  
                },
                
                 /*
                @description Función que inactiva una categoría 
                @author Iván Jojoa
                @date 30/01/2019
                */
                disabledCategory: function(id){                   
                    var data={
                        option: "statusCategory",
                        id: id,
                        is_active: 0
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/categoryController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Inactivación exitosa");  
                                location.reload();  
                            }else{
                                alert("Error en inactivación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que activa una categoría
                @author Iván Jojoa
                @date 30/01/2019
                */
                enabledCategory: function(id){                   
                    var data={
                        option: "statusCategory",
                        id: id,
                        is_active: 1
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/categoryController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Activación exitosa");
                                location.reload();      
                            }else{
                                alert("Error en activación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que actualiza un proveedor
                @author Orlando Mavisoy Guerrero
                @date 16/01/2019
                */
                updateCategoryData: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/categoryController.php",               
                        success: function (data) {
                            if(data){                               
                                alert("Registro actualizado exitosamente");             
                                location.reload();
                            }                                                       
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }
            });

/*
            @description Envia los datos de categoría para se almacenados en database
            @author Iván Jojoa
            @date 30/01/2019
            */
            $("#accept").on('click', function() {
                $id = $("#idcat").val();
                if($id.trim()===null || $id.trim()===undefined || $id.trim()===""){
                    data={
                        name : $("#name").val(),
                        option: "insert"
                    }         
                    $.addCategory(data);
                }else{
                    data={
                        id: $("#idcat").val(),
                        name : $("#name").val(),
                        option: "update"
                    }         
                    $.updateCategoryData(data);
                }                
                
            });       
        });
