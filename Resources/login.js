/**
         * @deprecated
         * @version 1.0        
         * @description Validar inicio de sesión
         * @author Orlando Mavisoy Guerrero
         * @date 26/12/2018
         */ 
        $(document).ready(function(){
            errorlogin = document.getElementById('errorlogin');
            $("#user").focus();    

            $.extend({
                login: function(data){
                    $.ajax({
                        type: "POST",
                        data: data,  
                        url: "./Controller/loginController.php",     
                       // dataType: "json",              
                        success: function (data) {	
                           console.log(data); 
                           alert('Acceso exitoso');                  
                        },
                        error : function(data) {   
                            console.log("data");                         
                            alert('Disculpe, existió un problema');
                        },
                    })
                    .done(function(data) {
                        console.log(data);
                    })
                    .fail(function() {
                        console.log('Error');   })
                    .always(function() {
                        console.log("complete");
                    });
                }               
            });          
                             
            $("#enter").on('click', function() {
                var user = $("#user").val();
                var pass = $("#password").val();                
                var data={
                    option: "enter",
                    user: user,
                    pass: pass,					
                };
        
                if(user.length=="")
                    {
                        errorlogin.innerHTML = "El campo usuario es obligatorio";	
                        errorlogin.style.display="block";
                        $("#user").focus(); 
                    }else if(pass.length=="")
                    {
                        errorlogin.innerHTML = "El campo contraseña es obligatorio";	
                        errorlogin.style.display="block";
                        $("#password").focus(); 
                    }else{
        
                        errorlogin.innerHTML = "";	
                        errorlogin.style.display="none";	
                        $.login(data);        
                    }				
            });
        
            //@description Función para entrar al sistema por medio del enter
            //@author Orlando Mavisoy Guerrero
            // $("body").on("keypress", function(event){       
            //     if (event.keyCode == 13 ) {
            //        Login();
            //     }
        
            // });
        
            //@description Limpia el campo usuario cuando se digita caracteres
            //@author Orlando Mavisoy Guerrero
            $("#user").on("keyup",function(){		
                errorlogin.innerHTML="";
                errorlogin.style.display="none";	    
            });
        
            //@description Limpia el campo contraseña cuando se digita caracteres
            //@author Orlando Mavisoy Guerrero
            $("#password").on("keyup",function(){	
                errorlogin.innerHTML="";
                errorlogin.style.display="none";	    
            });
        });
        
        
        
        
        
        