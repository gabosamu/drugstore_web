<?php   
        require_once 'Template/header.php';
        require_once '../Model/Dao/administratorDao.php';
?>
  
<div class="container">
  <h1>Administradores</h1>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAdmon"> + </button> 
    <!-- Modal -->
    <div id="modalAdmon" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Registro administrador</h4>
        </div>
        <div class="modal-body">
        <form>
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="name">Nombres <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="name">
                </div>
                <div class="form-group col-md-6">
                    <label for="lastname">Apellidos <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="lastname">
                </div>
            </div>           
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="tipdoc">Tipo de documento <span class="field_required">*</span></label>
                    <select required class="form-control" id="tipdoc">
                        <option value="">Seleccione</option>
                        <option value="CC">Cedula de ciudadania</option>
                        <option value="CE">Cedula de extranjeria</option>  
                        <option value="NIT">Nit</option>          
                        <option value="Otro">Otro tipo de documento</option>        
                    </select>
                </div> 
                <div class="form-group col-md-6">
                    <label for="numdoc">Número de documento <span class="field_required">*</span></label>
                    <input type="number" required class="form-control" id="numdoc">
                </div>
            </div>           
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="gender">Género <span class="field_required">*</span></label>
                    <select required class="form-control" id="gender">
                        <option value="">Seleccione</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>                
                    </select>    
                </div>
                <div class="form-group col-md-6">
                    <label for="address">Dirección <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="address">
                </div>          
            </div>           
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="city">Ciudad <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="city">
                </div>
                <div class="form-group col-md-6">
                    <label for="telephone">Teléfono </label>
                    <input type="number" class="form-control" id="telephone" >
                </div>
            </div>            
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="cellphone">Celular <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="cellphone">
                </div>
                <div class="form-group col-md-6">
                    <label for="email">Email <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="email">
                </div>
            </div>            
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="username">Usuario <span class="field_required">*</span></label>
                    <input type="text" required class="form-control" id="username">
                </div>
                <div class="form-group col-md-6">
                    <label for="password">Contraseña <span class="field_required">*</span></label>
                    <input type="password" required class="form-control" id="password">
                </div>
            </div>           
            <div class="form-group">
                <div class="form-group col-md-6">
                    <label for="confirm-password">Confirme contraseña <span class="field_required">*</span></label>
                    <input type="password" required class="form-control" id="confirm-password">
                </div>   
                <div class="form-group col-md-6">
                    <label for="profile">Perfil <span class="field_required">*</span></label>
                    <select required class="form-control" id="profile">                       
                        <option value="Administrador">Administrador</option>                                       
                    </select>
                </div>      
                <div class="form-group col-md-6">
                    <input type="hidden" required class="form-control" id="idadmon">                     
                </div>          
            </div> 
                                
            <button type="button" class="btn btn-primary" id="accept">Aceptar</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

 <!-- Datatables-->
  <table id="articlesDT" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>     
                <th>Apellido</th>
                <th>Número de documento</th>
                <th>Celular</th>               
                <th>Email</th>
                <th>Acción</th>
                <th>Estado</th>      
            </tr>
        </thead>        
        <tbody id="tBodyAdministrator">
                <?php 
                    $administrator = new administratorDao();
                    $res=json_decode($administrator->searchUserAll());
                    echo $res->option;
                ?>                 
        </tbody> 
    </table>
</div>
<script src="../Resources/js/administrator.js"></script>
<?php include 'Template/footer.php';?>