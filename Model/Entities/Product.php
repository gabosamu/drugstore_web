<?php
	class Product{
		public $id;
		public $image;
		public $barcode;
		public $name;
		public $description;
		public $inventary_min;
		public $price_in;
		public $price_out;
		public $unit;
		public $presentation;
		public $user_id;
		public $category_id;
		public $created_at;
		public $is_active;

		public Product($id, $image, $barcode, $name, $description, $inventary_min, $price_in, $price_out, $unit, $presentation, $user_id, $category_id, $created_at, $is_active)
		{
			$this->id = $id;
			$this->image = $image;
			$this->barcode = $barcode;
			$this->name = $name;
			$this->description = $description;
			$this->inventary_min = $inventary_min;
			$this->price_in= $price_in;
			$this->price_out = $price_out;
			$this->unit = $unit;
			$this->presentation = $presentation;
			$this->user_id= $user_id;
			$this->category_id = $category_id;
			$this->created_at = $created_at;	
			$this->is_active = $is_active;									

		}
		public function getId()
		{
		    return $this->id;
		}
		
		public function setId($id)
		{
		    $this->id = $id;
		    return $this;
		}

		public function getImage()
		{
		    return $this->image;
		}
		
		public function setImage($image)
		{
		    $this->image = $image;
		    return $this;
		}

		public function getBarcode()
		{
		    return $this->barcode;
		}
		
		public function setBarcode($barcode)
		{
		    $this->barcode = $barcode;
		    return $this;
		}

		public function getName()
		{
		    return $this->name;
		}
		
		public function setName($name)
		{
		    $this->name = $name;
		    return $this;
		}

		public function getDescription()
		{
		    return $this->description;
		}
		
		public function setDescription($description)
		{
		    $this->description= $description;
		    return $this;
		}

		public function getInventary_min()
		{
		    return $this->inventary_min;
		}
		
		public function setInventary_min($inventary_min)
		{
		    $this->inventary_min = $inventary_min;
		    return $this;
		}

		public function getPrice_in()
		{
		    return $this->price_in;
		}
		
		public function setPrice_in($price_in)
		{
		    $this->price_in = $price_in;
		    return $this;
		}

		public function getPrice_out()
		{
		    return $this->price_out;
		}
		
		public function setPrice_out($price_out)
		{
		    $this->price_out = $price_out;
		    return $this;
		}

		public function getUnit()
		{
		    return $this->unit;
		}
		
		public function setUnit($unit)
		{
		    $this->unit = $unit;
		    return $this;
		}

		public function getPresentation()
		{
		    return $this->presentation;
		}
		
		public function setPresentation($presentation)
		{
		    $this->presentation = $presentation;
		    return $this;
		}

		public function getUser_id()
		{
		    return $this->user_id;
		}
		
		public function setUser_id($user_id)
		{
		    $this->user_id = $user_id;
		    return $this;
		}

		public function getCategory_id()
		{
		    return $this->category_id;
		}
		
		public function setCategory_id($category_id)
		{
		    $this->category_id = $category_id;
		    return $this;
		}

		public function getCreated_at()
		{
		    return $this->created_at;
		}
		
		public function setCreated_at($created_at)
		{
		    $this->created_at = $created_at;
		    return $this;
		}

		public function getIs_active()
		{
		    return $this->is_active;
		}
		
		public function setIs_active($is_active)
		{
		    $this->is_active = $is_active;
		    return $this;
		}



		
		

	}
?>