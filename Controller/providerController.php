<?php
	require_once("../Model/Dao/providerDao.php");  
	require_once("../Model/Entities/Provider.php");
	
	//  * @description Metodo que envia recibe y envia parametros del proveedor	
	//  * @author Orlando Mavisoy Guerrero
	//  * @date 09/01/2019	
	     
	$Option = $_POST['option'];	
	if($Option=="insert"){
		$provider = new Provider();
		
		$provider->name = $_POST['name'];
		$provider->nit =	$_POST['nit'];	
        $provider->address =$_POST['address'];
        $provider->city = $_POST['city'];	
        $provider->phone=  $_POST['phone'];
        $provider->cellphone = $_POST['cellphone'];
        $provider->email = $_POST['email'];
        $provider->create_at = $_POST['fCreation'];
		$provider->status = 1;

		$providerDao = new providerDao();
		$sql =$providerDao->insert($provider);	
		echo $sql;		
	}

	//  * @description Metodo que recibe los datos del proveedor editar el estado del proveedor	
	//  * @author Orlando Mavisoy Guerrero
	//  * @date 12/01/2019	
	     
	if($Option=="statusProvider"){
		$provider = new Provider();
		$provider->idprov =  $_POST['idprov'];
		$provider->status =  $_POST['status'];		

		$providerDao = new providerDao();
		$sql = $providerDao->statusProvider($provider);	
		echo $sql;		
	}	

	//  * @description Metodo que recibe los datos del proveedor para actualizar	
	//  * @author Orlando Mavisoy Guerrero
	//  * @date 16/01/2019	

	if($Option=="update"){
		$provider = new Provider();

		$provider->idprov = $_POST['idprov'];
		$provider->name = $_POST['name'];
		$provider->nit =	$_POST['nit'];	
        $provider->address =$_POST['address'];
        $provider->city = $_POST['city'];	
        $provider->phone=  $_POST['phone'];
        $provider->cellphone = $_POST['cellphone'];
        $provider->email = $_POST['email'];
        $provider->create_at = date('Y-m-d H:i:s');
		$provider->status = 1;

		$providerDao = new providerDao();
		$sql =$providerDao->update($provider);	
		echo $sql;		
	}

	
?>