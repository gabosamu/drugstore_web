       /**          
                 * @description Función que envia mediante ajax los datos del articulo
                 * @author Ivan Jojoa
                 * @date 17/01/2019
                 */ 
            $(document).ready(function(){      

             $.extend({             
                addInvoices: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/articleController.php",               
                        success: function (data) {
                            if(data==="1"){
                                alert("Registro exitoso");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                },

                /**          
                 * @description Función que recibe un parametro para calcular el total precio de costo
                 * @author Orlando Mavisoy Guerrero
                 * @date 23/01/2019
                */ 
       
                addPriceCost: function () {  
                    var total = 1;
                    var change= false; //
                    $(".priceCost").each(function(){
                        if (!isNaN(parseFloat($(this).val()))) {
                            change= true;
                            total *= parseFloat($(this).val());                           
                        }
                    });
                    // Si se modifico el valor , retornamos la multiplicación
                    // caso contrario 0
                    total = (change)? total:0;
                    $("#totalPriceCost").val(total);                    
                },

                addPriceSale: function(){
                    $can = $("#quantity").val();
                    var totalpv = 1;
                    var changepv= false;
                    $(".priceSale").each(function(){
                        if (!isNaN(parseFloat($(this).val()))) {
                            changepv= true;
                            $can *= parseFloat($(this).val());
                        }
                    });
                    // Si se modifico el valor , retornamos la multiplicación
                    // caso contrario 0
                    totalpv = (changepv)? totalpv:0;
                    $("#totalPriceSale").val(totalpv);
                }
            });                 
           
            /**          
             * @description Función recibe los datos del articulo
             * @author Ivan Jojoa
             * @date 11/01/2019
            */ 
                                       
            $("#accept").on('click', function() { 
                                                      
                        data={
                            idProv : $("#idProv").val(),                           
                            idArt : $("#idArt").val(),
                            idCat: $("#idCat").val(),
                            idUnity : $("#idUnity").val(),
                            idLab : $("#idLab").val(),
                            bill : $("#bill").val(),                                                                                
                            lot: $("#lot").val(),                                  
                            quantity : $("#quantity").val(),                           
                            costPrice : $("#costPrice").val(),
                            salePrice : $("#salePrice").val(),                                             
                            iva : $("#iva").val(),
                            expirationDate: $("#expirationDate").val(),
                            totalPriceCost: $("#totalPriceCost").val(), 
                            totalPriceSale: $("#totalPriceSale").val(),      
                            typeInvoice: $("#typeInvoice").val(),                                               
                            option: "insert"
                        }                      
                        $.addInvoices(data);                                  
            });            
            
        });
        
        
        
        
        
        