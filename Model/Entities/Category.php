<?php
	class Category{
		public $id;
		
		public $name;	
		public $created_at;
		public $is_active;
	
		public function getId()
		{
		    return $this->id;
		}
		
		public function setId($id)
		{
		    $this->id = $id;
		    return $this;
		}		

		public function getName()
		{
		    return $this->name;
		}
		
		public function setName($name)
		{
		    $this->name = $name;
		    return $this;
		}		
		
		/**
		 * Get the value of is_active
		 */ 
		public function getIs_active()
		{
				return $this->is_active;
		}

		/**
		 * Set the value of is_active
		 *
		 * @return  self
		 */ 
		public function setIs_active($is_active)
		{
				$this->is_active = $is_active;

				return $this;
		}

		/**
		 * Get the value of created_at
		 */ 
		public function getCreated_at()
		{
				return $this->created_at;
		}

		/**
		 * Set the value of created_at
		 *
		 * @return  self
		 */ 
		public function setCreated_at($created_at)
		{
				$this->created_at = $created_at;

				return $this;
		}
	}
?>