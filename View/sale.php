<?php 
    require_once 'Template/header.php';
    require_once '../Model/Dao/saleDao.php';
?>
  
<div class="container">
  <h1>Factura de Venta</h1>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalSale"> + </button> 
    <!-- Modal -->
    <div id="modalSale" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Factura de Venta</h4>
        </div>
        <div class="modal-body">
        <form id="form">
            <div class="form-group">
                <label for="idClient">Nombre del Cliente <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="idClient">
            </div>
            <div class="form-group">
                <label for="nameAdmin">Nombre del administrador <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="nameAdmin">
            </div>
            <div class="form-group">
                <label for="numberInvoice">Número de Factura <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="numberInvoice" >
            </div>

            <div class="form-group">
                <label for="idInvoice">Factura<span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="idInvoice" >
            </div>
            <div class="form-group">
                <label for="cost">Costo <span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="cost">
            </div>
            <div class="form-group">
                <label for="quantity">Cantidad<span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="quantity" >
            </div>
            <div class="form-group">
                <label for="discount">Descuento<span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="discount" >
            </div>

            <div class="form-group">
                <label for="expiration">Fecha de Expiración <span class="field_required">*</span></label>
                <input type="date" required class="form-control" id="expiration" >
            </div>  

            <div class="form-group">
                <label for="total">Total<span class="field_required">*</span></label>
                <input type="text" required class="form-control" id="total" >
            </div>
            <div class="form-group">               
                <input type="hidden" class="form-control" id="id">
            </div>  
           
            <button type="button"  class="btn btn-danger" id="accept">Aceptar</button>
        </form>
        </div>
        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

    <!-- Datatables-->
    <table id="saleDT" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre del Cliente</th>     
                
                <th>Número de Factura</th>
                <th>Factura</th>
                <th>Costo</th>               
                <th>Cantidad</th>
                <th>Total</th>
                <th>Fecha</th>
                
                <th>Acción</th>
                <th>Estado</th>
            </tr>
        </thead>        
        <tbody id="tBodySale">
                <?php 
                    $sale = new saleDao();
                    $res=json_decode($sale->searchSaleAll());
                    echo $res->option;
                ?>  
        </tbody> 
    </table>
</div>
<script src="../Resources/js/sale.js"></script>
<?php include 'Template/footer.php';?>