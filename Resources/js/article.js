/**
         * @deprecated
         * @version 1.0        
         * @description Validar inicio de sesión
         * @author Iván Jojoa
         * @date 30/01/2019
         */ 
        $(document).ready(function(){      
            $id = "";
            $.extend({
                 /*
                @description Funcion que envia los datos de artículos para ser almacenados en base de datos
                @author Iván Jojoa
                @date 30/01/2019
                */
                addArticle: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/articleController.php",               
                        success: function (data) {
                             if((data==="") || (data==="false")){				   	
                                alert("El Artículo ya existe");                                
                            }else{
                                alert("Registro exitoso");				
                                location.reload();
                            }														
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }, 

                /*
                @description actualiza los datos de artículos
                @author Iván Jojoa
                @date 01/02/2019
                */
                updateArticle: function(id, nameArticle, reference, invimaReg,  idCat, create_at){            
                    $('#modalArticle').modal('show');                                   
                    $("#nameArticle").val(nameArticle);
                    $("#referen").val(reference);
                    $("#invimaReg").val(invimaReg);                   
                    $("#idCat").val(idCat); 
                    $("#idart").val(id);                  
                },
                
                 /*
                @description Función que inactiva un artículo
                @author Iván Jojoa
                @date 12/01/2019
                */
                disabledArticle: function(id){                   
                    var data={
                        option: "statusArticle",
                        id: id,
                        is_active: 0
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/articleController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Inactivación exitosa");  
                                location.reload();  
                            }else{
                                alert("Error en inactivación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que activa un artículo
                @author Iván Jojoa
                @date 01/02/2019
                */
                enabledArticle: function(id){                   
                    var data={
                        option: "statusArticle",
                        id: id,
                        is_active: 1
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/articleController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Activación exitosa");
                                location.reload();      
                            }else{
                                alert("Error en activación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que actualiza un artículo
                @author Iván Jojoa
                @date 01/02/2019
                */
                updateArticleData: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/articleController.php",               
                        success: function (data) {
                            if(data){                               
                                alert("Registro actualizado exitosamente");             
                                location.reload();
                            }                                                       
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }       
            });          
                        
            /*
            @description Envia los datos de artículos para se almacenados en database
            @author Iván Jojoa
            @date 10/01/2019
            */
            $("#accept").on('click', function() {
                $id= $("#idart").val();
                if($id.trim()===null || $id.trim()===undefined || $id.trim()===""){
                    data={
                        nameArticle : $("#nameArticle").val(),
                        referen : $("#referen").val(),
                        invimaReg : $("#invimaReg").val(),                        
                        idCat: $("#idCat").val(),
                        option: "insert"
                    }         
                    $.addArticle(data);
                }else{
                    data={
                        id: $("#idart").val(),
                        nameArticle : $("#nameArticle").val(),
                        referen : $("#referen").val(),
                        invimaReg : $("#invimaReg").val(),                 
                        idCat: $("#idCat").val(),
                        option: "update"
                    }         
                    $.updateArticleData(data);
                }         
            });        
        });
        
        
        
        
        
        