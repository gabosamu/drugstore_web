/**
         * @deprecated
         * @version 1.0        
         * @description Validar inicio de sesión
         * @author Iván Jojoa
         * @date 22/01/2019
         */ 
         $(document).ready(function(){    
          
            $.extend({
                 /*
                @description Funcion que envia los datos de laboratorios para ser almacenados en base de datos
                @author Iván Jojoa
                @date 22/01/2019
                */
                addLaboratory: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/laboratoryController.php",               
                        success: function(data){
                            if((data==="") ||(data==="false")){
                                alert("El laboratorio ya existe")
                            }else{
                                alert("Registro exitoso");
                                location.reload();
                            }
                        },        
                        error : function(data){
                            console.log(data)
                            alert('Disculpe, existió un problema en el sistema'.data);
                        },
                    });
                }, 
                /*
                @description actualiza los datos de laboratorios 
                @author Iván Jojoa
                @date 31/01/2019
                */
                updateLaboratory: function(id, name){            
                    $('#modalLaboratory').modal('show');                                   
                    $("#name").val(name);                 
                    $("#idlab").val(id);                  
                },
                
                 /*
                @description Función que inactiva un laboratorio
                @author Iván Jojoa
                @date 31/01/2019
                */
                disabledLaboratory: function(id){                   
                    var data={
                        option: "statusLaboratory",
                        id: id,
                        status: 0
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/laboratoryController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Inactivación exitosa");  
                                location.reload();  
                            }else{
                                alert("Error en inactivación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que activa un laboratorio
                @author Iván Jojoa
                @date 31/01/2019
                */
                enabledLaboratory: function(id){                   
                    var data={
                        option: "statusLaboratory",
                        id: id,
                        status: 1
                    }
        
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "../Controller/laboratoryController.php",     
                        success: function (data) {
                            if(data==="1"){
                                alert("Activación exitosa");
                                location.reload();      
                            }else{
                                alert("Error en activación");
                            }                                                                   
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });         
                },

                /*
                @description Función que actualiza un laboratorio
                @author Iván Jojoa
                @date 31/01/2019
                */
                updateLaboratoryData: function(data){
                    $.ajax({
                        type: 'POST',
                        data: data,            
                        url: "../Controller/laboratoryController.php",               
                        success: function (data) {                                                        
                                alert("Registro actualizado exitosamente");             
                                location.reload();                                                                                
                        },
                        error : function(data) {   
                            console.log(data);                         
                            alert('Disculpe, existió un problema en el sistema' . data); 
                        },
                    });
                }      
            });

            /*
            @description Envia los datos de laboratorios para se almacenados en database
            @author Iván Jojoa
            @date 31/01/2019
            */
            $("#accept").on('click', function() {
                $id = $("#idlab").val();
                if($id.trim()===null || $id.trim()===undefined || $id.trim()===""){
                    data={
                        name : $("#name").val(),                        
                        option: "insert"
                    }         
                    $.addLaboratory(data);
                }else{
                    data={
                        id: $("#idlab").val(),
                        name : $("#name").val(),                        
                        option: "update"
                    }         
                    $.updateLaboratoryData(data);
                }                
                
            });        
        });
        
        